package com.lmccarty.trailersearch.fragment;

import java.util.ArrayList;
import java.util.List;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.lmccarty.trailersearch.R;
import com.lmccarty.trailersearch.model.FavoriteMovie;
import com.lmccarty.trailersearch.model.VolleySingleton;
import android.app.Activity;
import android.app.ListFragment;
import android.content.Context;
import android.os.AsyncTask;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class FavoriteMovies extends ListFragment implements OnItemLongClickListener {

	private RequestQueue queue;
	private ImageLoader imageLoader;

	private BaseAdapter adapter;
	private GetFavsTask mGetFavsTask;

	private static List<Object> theMDbID = new ArrayList<Object>();
	private static List<Object> movies = new ArrayList<Object>();
	private static List<Object> thumbs = new ArrayList<Object>();

	private TextView txtEmpty;

	private int longPressedIndex = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
		getActivity().getActionBar().setHomeButtonEnabled(true);
		getActivity().getActionBar().setSubtitle(
				getActivity().getResources().getString(R.string.fav_movies));

		queue = VolleySingleton.getInstance(getActivity()).getRequestQueue();
		adapter = new MovieListAdapter(getActivity());
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.panel_main, container, false);

		txtEmpty = (TextView) view.findViewById(R.id.txtEmpty);

		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		getListView().setLongClickable(true);
		getListView().setOnItemLongClickListener(FavoriteMovies.this);
		getListView().setAdapter(adapter);
	}

	OnFavMoviesClicked mCallback;

	public interface OnFavMoviesClicked {

		public void showMovieDialog(String theMDbID);

		public void movieTitleClicked(long theMDbID);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mCallback = (OnFavMoviesClicked) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnFavActorsClicked");
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		mGetFavsTask = (GetFavsTask) new GetFavsTask().execute();
	}

	@Override
	public void onStop() {
		super.onStop();
		mGetFavsTask.getStatus();
		if (mGetFavsTask != null && mGetFavsTask.getStatus() == Status.RUNNING) {
			mGetFavsTask.cancel(true);
			mGetFavsTask = null;
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		queue.cancelAll(this);
	}

	static class ViewHolder {
		TextView txtTitle;
		ImageView imgFav;
	}

	private class MovieListAdapter extends BaseAdapter {
		public MovieListAdapter(Context context) {
			mContext = context;
		}

		public int getCount() {
			return movies.size();
		}

		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(final int position, View convertView, ViewGroup parent) {

			if (convertView == null) {
				convertView = LayoutInflater.from(mContext).inflate(
						R.layout.favs_list_item, null);
			}

			final ViewHolder holder = new ViewHolder();

			holder.txtTitle = (TextView) convertView.findViewById(R.id.txtTitle);
			holder.txtTitle.setText(movies.get(position).toString());

			NetworkImageView image = (NetworkImageView) convertView
					.findViewById(R.id.imgMovie);
			imageLoader = VolleySingleton.getInstance(getActivity()).getImageLoader();

			String sImage = thumbs.get(position).toString();

			image.setDefaultImageResId(R.drawable.no_image);
			image.setImageUrl(sImage, imageLoader);

			return convertView;
		}

		private Context mContext;
	}

	private class GetFavsTask extends AsyncTask<Void, Void, List<List<Object>>> {
		@Override
		protected void onPostExecute(List<List<Object>> param) {

			movies = param.get(0);
			thumbs = param.get(1);
			theMDbID = param.get(2);

			adapter.notifyDataSetChanged();

			if (adapter.getCount() == 0)
				txtEmpty.setVisibility(View.VISIBLE);
		}

		@Override
		protected List<List<Object>> doInBackground(Void... arg0) {
			return FavoriteMovie.getFavoriteMovies(getActivity());
		}
	}

	@Override
	public void onListItemClick(ListView l, View v, int pos, long id) {
		super.onListItemClick(l, v, pos, id);
		mCallback.movieTitleClicked((Long) theMDbID.get(pos));
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		longPressedIndex = arg2;
		mCallback.showMovieDialog(String.valueOf(theMDbID.get(arg2)));
		return true;
	}

	public void resetList() {
		theMDbID.remove(longPressedIndex);
		movies.remove(longPressedIndex);
		thumbs.remove(longPressedIndex);

		adapter.notifyDataSetChanged();

		if (adapter.getCount() == 0)
			txtEmpty.setVisibility(View.VISIBLE);
	}
}