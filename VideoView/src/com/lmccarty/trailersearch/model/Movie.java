package com.lmccarty.trailersearch.model;

public class Movie {

	private long id;
	private int year;
	private String title;
	private String mpaa_rating;
	private String runtime;
	private String critics_consensus;
	private String synopsis;
	private ReleaseDates release_dates;
	private Ratings ratings;
	private Posters posters;
	private AlternateIDs alternate_ids;
	private Links links;
	private AbridgedCast[] abridged_cast;

	public Movie() {
	}

	public void setID(long sid) {
		this.id = sid;
	}

	public long getID() {
		return id;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getYear() {
		return year;
	}

	public void setRating(String rating) {
		this.mpaa_rating = rating;
	}

	public String getRating() {
		return mpaa_rating;
	}

	public void setRuntime(String time) {
		this.runtime = time;
	}

	public String getRuntime() {
		return runtime;
	}

	public void setCritics(String cc) {
		this.critics_consensus = cc;
	}

	public String getCritics() {
		return critics_consensus;
	}

	public void setSynopsis(String synopsis) {
		this.synopsis = synopsis;
	}

	public String getSynopsis() {
		return synopsis;
	}

	public void setRelease(ReleaseDates release) {
		this.release_dates = release;
	}

	public ReleaseDates getReleaseDates() {
		return release_dates;
	}

	public void setRatings(Ratings ratings) {
		this.ratings = ratings;
	}

	public Ratings getRatings() {
		return ratings;
	}

	public void setPosters(Posters posters) {
		this.posters = posters;
	}

	public Posters getPosters() {
		return posters;
	}

	public void setAlternateIDs(AlternateIDs ids) {
		this.alternate_ids = ids;
	}

	public AlternateIDs getAlternateIDs() {
		return alternate_ids;
	}

	public void setLinks(Links links) {
		this.links = links;
	}

	public Links getLinks() {
		return links;
	}

	public void setCast(AbridgedCast[] cast) {
		this.abridged_cast = cast;
	}

	public AbridgedCast[] getCast() {
		return abridged_cast;
	}

	public class Posters {

		private String thumbnail;
		private String profile;
		private String detailed;
		private String original;

		public void setThumbnail(String thumb) {
			this.thumbnail = thumb;
		}

		public String getThumbnail() {
			return thumbnail;
		}

		public void setProfile(String profile) {
			this.profile = profile;
		}

		public String getProfile() {
			return profile;
		}

		public void setDetailed(String detailed) {
			this.detailed = detailed;
		}

		public String getDetailed() {
			return detailed;
		}

		public void setOriginal(String original) {
			this.original = original;
		}

		public String getOriginal() {
			return original;
		}
	}

	public class AlternateIDs {

		private String imdb;

		public void setIMDB(String id) {
			this.imdb = id;
		}

		public String getIMDB() {
			return imdb;
		}
	}

	public class Links {

		private String self;
		private String alternate;
		private String cast;
		private String clips;
		private String reviews;
		private String similar;

		public void setSelf(String self) {
			this.self = self;
		}

		public String getSelf() {
			return self;
		}

		public void setAlternate(String alt) {
			this.alternate = alt;
		}

		public String getAlternate() {
			return alternate;
		}

		public void setCast(String cast) {
			this.cast = cast;
		}

		public String getCast() {
			return cast;
		}

		public void setClips(String clips) {
			this.clips = clips;
		}

		public String getClips() {
			return clips;
		}

		public void setReviews(String reviews) {
			this.reviews = reviews;
		}

		public String getReviews() {
			return reviews;
		}

		public void setSimilar(String sim) {
			this.similar = sim;
		}

		public String getSimilar() {
			return similar;
		}
	}

	public class AbridgedCast {

		public AbridgedCast() {
		}

		private String name;
		private long id;

		public void setName(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}

		public void setID(long id) {
			this.id = id;
		}

		public long getID() {
			return id;
		}
	}

	public class Ratings {

		private int critics_score;
		private int audience_score;
		private String critics_rating;
		private String audience_rating;

		public Ratings() {
		}

		public void setCriticsScore(int score) {
			this.critics_score = score;
		}

		public int getCriticsScore() {
			return critics_score;
		}

		public void setAudienceScore(int score) {
			this.audience_score = score;
		}

		public int getAudienceScore() {
			return audience_score;
		}

		public void setCriticsRating(String rating) {
			this.critics_rating = rating;
		}

		public String getCriticsRating() {
			return critics_rating;
		}

		public void setAudienceRating(String rating) {
			this.audience_rating = rating;
		}

		public String getAudienceRating() {
			return audience_rating;
		}
	}

	public class ReleaseDates {

		private String theater;
		private String dvd;

		public ReleaseDates() {
		}

		public void setTheater(String t) {
			theater = t;
		}

		public String getTheater() {
			return theater;
		}

		public void setDvd(String d) {
			dvd = d;
		}

		public String getDvd() {
			return dvd;
		}
	}
}