package com.lmccarty.trailersearch.model;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import com.lmccarty.trailersearch.dao.DAOFactory;
import com.lmccarty.trailersearch.dao.FavoriteMovieDAO;

public class FavoriteMovie {

	private long theMDBID;
	private long rtID;
	private String imdbID;
	private String title;
	private String thumb;

	private static DAOFactory daoFactory = DAOFactory.getInstance();

	public FavoriteMovie(String imdbID, long rtID, String title, String thumb) {
		this(imdbID, rtID);
		this.title = title;
		this.thumb = thumb;
	}

	public FavoriteMovie(String imdbID, long rtID, long theMDBID, String title, String thumb) {
		this(imdbID, rtID, title, thumb);
		this.theMDBID = theMDBID;
	}

	public FavoriteMovie(long theMDBID, String imdbID, String title, String thumb) {
		this(theMDBID);
		this.imdbID = imdbID;
		this.title = title;
		this.thumb = thumb;
	}

	public FavoriteMovie(String imdbID, long rtID) {
		this.imdbID = imdbID;
		this.rtID = rtID;
	}

	public FavoriteMovie(long theMDBID) {
		this.theMDBID = theMDBID;
	}

	public long getTheMDBID() {
		return theMDBID;
	}

	public long getRTID() {
		return rtID;
	}

	public String getIMDBID() {
		return imdbID;
	}

	public String getTitle() {
		return title;
	}

	public String getThumb() {
		return thumb;
	}

	/**
	 * @param ctx
	 *            app context
	 * @param imdbID
	 *            IMDb ID
	 * @param rtID
	 *            Rotten Tomatoes ID
	 * @param theMDBID
	 * @param title
	 *            Movie Title
	 * @param thumb
	 *            Movie thumbnail path
	 */
	public static void add(Context ctx, String imdbID, long rtID, long theMDBID,
			String title, String thumb) {
		FavoriteMovieDAO dao = null;
		try {
			dao = daoFactory.getFavoriteMovieDAO(ctx);
			dao.addFavoriteMovie(new FavoriteMovie(imdbID, rtID, theMDBID, title, thumb));
		} finally {
			if (dao != null)
				dao.close();
		}
	}

	public static void add(Context ctx, long theMDBID, String imdbID, String title,
			String thumb) {
		FavoriteMovieDAO dao = null;
		try {
			dao = daoFactory.getFavoriteMovieDAO(ctx);
			dao.addFavoriteTheMDBMovie(new FavoriteMovie(theMDBID, imdbID, title, thumb));
		} finally {
			if (dao != null)
				dao.close();
		}
	}

	public static boolean isFavoriteMovie(Context ctx, String movieID) {
		FavoriteMovieDAO dao = null;
		boolean favorite = false;
		try {
			dao = daoFactory.getFavoriteMovieDAO(ctx);
			favorite = dao.isFavoriteMovie(movieID);
		} finally {
			if (dao != null)
				dao.close();
		}
		return favorite;
	}

	public static boolean isFavoriteMovieCheckIMDB(Context ctx, String movieID) {
		FavoriteMovieDAO dao = null;
		boolean favorite = false;
		try {
			dao = daoFactory.getFavoriteMovieDAO(ctx);
			favorite = dao.isFavoriteMovieCheckIMDB(movieID);
		} finally {
			if (dao != null)
				dao.close();
		}
		return favorite;
	}

	public static void delete(Context ctx, String imdbID, long rtID) {
		FavoriteMovieDAO dao = null;
		try {
			dao = daoFactory.getFavoriteMovieDAO(ctx);
			dao.deleteFavoriteMovie(new FavoriteMovie(imdbID, rtID));
		} finally {
			if (dao != null)
				dao.close();
		}
	}

	public static void delete(Context ctx, long theMDBID) {
		FavoriteMovieDAO dao = null;
		try {
			dao = daoFactory.getFavoriteMovieDAO(ctx);
			dao.deleteFavoriteMovieFavsList(new FavoriteMovie(theMDBID));
		} finally {
			if (dao != null)
				dao.close();
		}
	}

	public static List<List<Object>> getFavoriteMovies(Context ctx) {
		FavoriteMovieDAO dao = null;
		List<List<Object>> movies = new ArrayList<List<Object>>();
		try {
			dao = daoFactory.getFavoriteMovieDAO(ctx);
			movies = dao.getFavoriteMovieList();
		} finally {
			if (dao != null) {
				dao.close();
			}
		}

		return movies;
	}
}
