package com.lmccarty.trailersearch.activity;

import com.lmccarty.trailersearch.R;
import com.lmccarty.trailersearch.fragment.SearchResults;
import com.lmccarty.trailersearch.model.OnMovieListClicked;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnActionExpandListener;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;

public class SearchResultsTitleActivity extends Activity implements OnActionExpandListener,
		OnQueryTextListener, OnMovieListClicked {

	private MenuItem menuItemTitle;
	private MenuItem menuItemActor;
	private SearchView searchViewTitle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		MainActivity.parents.push(getClass());
		
		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		String query = null;
		if (extras != null) {
			// TODO refactor
			query = extras.getString(MainActivity.MOVIE_QUERY);
			DataStrings.setMovieTitle(query);
		} else {
			// Returning from child activity
			query = DataStrings.getMovieTitle();
		}

		SearchResults fragResults = (SearchResults) getFragmentManager().findFragmentByTag(
				MainActivity.LIST_TITLE);

		if (fragResults == null || !fragResults.getClass().equals(SearchResults.class)) {
			FragmentManager manager = getFragmentManager();
			FragmentTransaction trans = manager.beginTransaction();

			SearchResults frag = new SearchResults();

			Bundle bundle = new Bundle();
			bundle.putString(MainActivity.MOVIE_QUERY, query);
			frag.setArguments(bundle);

			trans.replace(android.R.id.content, frag, MainActivity.LIST_TITLE).commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);

		menuItemTitle = menu.findItem(R.id.action_search_title);
		menuItemTitle.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		menuItemTitle.setVisible(true);

		menuItemActor = menu.findItem(R.id.action_search_actor);
		menuItemActor.setVisible(false);

		searchViewTitle = (SearchView) menuItemTitle.getActionView();
		searchViewTitle
				.setQueryHint(getResources().getString(R.string.action_search_title));
		searchViewTitle.setOnQueryTextListener(this);

		return true;
	}

	@Override
	public boolean onMenuItemActionCollapse(MenuItem arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
		getActionBar().setSubtitle(query);
		searchViewTitle.onActionViewCollapsed();
		SearchResults frag = (SearchResults) getFragmentManager().findFragmentByTag(
				MainActivity.LIST_TITLE);

		frag.doSearch(query);
		return true;
	}

	@Override
	public void movieTitleClicked(long movieID) {
		Intent intent = new Intent(this, DetailsDataActivity.class).putExtra("the_mdb_id",
				movieID);
		startActivity(intent);
	}

	@Override
	public boolean onQueryTextChange(String arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onMenuItemActionExpand(MenuItem arg0) {
		// TODO Auto-generated method stub
		return false;
	}
}
