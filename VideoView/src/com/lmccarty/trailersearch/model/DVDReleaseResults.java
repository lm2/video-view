package com.lmccarty.trailersearch.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DVDReleaseResults {

	private static Map<String, List<Object>> dvdReleaseResults;

	private DVDReleaseResults() {
	}

	public static void setDVDReleaseResults(HashMap<String, List<Object>> dvdData) {
		dvdReleaseResults = dvdData;
	}

	public static Map<String, List<Object>> getDVDReleaseResults() {
		return dvdReleaseResults;
	}
}
