package com.lmccarty.trailersearch.fragment;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.lmccarty.trailersearch.R;
import com.lmccarty.trailersearch.activity.MainActivity;
import com.lmccarty.trailersearch.model.FavoriteActor;
import com.lmccarty.trailersearch.model.OnActorListClicked;
import com.lmccarty.trailersearch.model.TmdbMovie;
import com.lmccarty.trailersearch.model.VolleySingleton;
import com.lmccarty.trailersearch.model.TmdbMovie.Cast;
import com.lmccarty.trailersearch.util.AppPrefs;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class ShowCast extends Fragment implements OnItemClickListener {

	private static final String TAG = ShowCast.class.getSimpleName();

	private String id;
	private String configUrl;
	private String title;

	private List<Boolean> favIsSelected;

	private List<String> actorName = new ArrayList<String>();
	private List<Long> actorID = new ArrayList<Long>();
	private List<String> characterName = new ArrayList<String>();
	private List<String> actorThumb = new ArrayList<String>();

	private ProgressBar progress;
	private ListView list;
	private ListAdapter adapter;
	private RequestQueue requestQueue;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestQueue = VolleySingleton.getInstance(getActivity()).getRequestQueue();
		adapter = new CastListAdapter(getActivity());

		if (savedInstanceState != null) {
			id = savedInstanceState.getString("id");
			title = savedInstanceState.getString("title");
		} else {
			Bundle args = this.getArguments();
			id = args.getString(MainActivity.MOVIE_ID);
			title = args.getString(MainActivity.MOVIE_TITLE);
		}

		configUrl = AppPrefs.getTmdbConfigUrl(getActivity());

		getActivity().getActionBar().setSubtitle(title);

		getData();
	}

	// Interface
	OnActorListClicked mCallback;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mCallback = (OnActorListClicked) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnFavActorsClicked");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.list_title, container, false);

		list = (ListView) view.findViewById(R.id.list);
		list.setOnItemClickListener(this);

		progress = (ProgressBar) view.findViewById(R.id.progList);

		return view;
	}

	@Override
	public void onSaveInstanceState(Bundle out) {
		super.onSaveInstanceState(out);
		out.putString("id", id);
		out.putString("title", title);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		requestQueue.cancelAll(this);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		mCallback.showMovieCredits(actorID.get(arg2), actorName.get(arg2));
	}

	private void getData() {
		String url = String.format(ApiStrings.getSingleMovie(), id,
				ApiStrings.getMovieDbKey());

		JsonObjectRequest objRequestCast = new JsonObjectRequest(Request.Method.GET, url,
				null, new ResponseListener(), new ErrorListener());
		requestQueue.add(objRequestCast);
	}

	// TODO refactor, parse JSON in JSONHelper
	private class ResponseListener implements Listener<JSONObject> {
		@Override
		public void onResponse(JSONObject response) {
			JsonParser parser = new JsonParser();
			JsonObject jsonObject = parser.parse(response.toString()).getAsJsonObject();
			JsonObject movies = null;
			if (jsonObject.has(MainActivity.CREDITS)) {
				movies = jsonObject.get(MainActivity.CREDITS).getAsJsonObject();
			}

			Gson gson = new Gson();

			try {

				Type movieType = new TypeToken<TmdbMovie>() {
				}.getType();

				TmdbMovie movie = gson.fromJson(movies, movieType);

				List<Long> id = new ArrayList<Long>();
				List<String> name = new ArrayList<String>();
				List<String> path = new ArrayList<String>();
				List<String> character = new ArrayList<String>();
				List<Boolean> selected = new ArrayList<Boolean>();

				for (Cast data : movie.getCast()) {
					name.add(data.getName());

					// TODO long needed
					Long aID = Long.valueOf(data.getID());

					id.add(aID);
					path.add(configUrl + data.getPath());
					character.add(data.getCharacter());

					//selected.add(FavoriteActor.isFavoriteActor(getActivity(), aID));
				}

				actorName = name;
				actorID = id;
				actorThumb = path;
				characterName = character;
				favIsSelected = selected;

				list.setAdapter(adapter);
				if (progress.getVisibility() == View.VISIBLE)
					progress.setVisibility(View.GONE);

			} catch (JsonSyntaxException e) {
				Log.e(TAG, "ERROR: " + e.getMessage());
			}
		}
	}

	private class ErrorListener implements Response.ErrorListener {
		@Override
		public void onErrorResponse(VolleyError error) {
			Log.e(TAG, "RESPONSE ERROR: " + error.toString());
		}
	}

	static class ViewHolder {
		TextView txtTitle;
		TextView txtDesc;
		// NetworkImageView image;
		ImageView imgFav;
	}

	private class CastListAdapter extends BaseAdapter {
		public CastListAdapter(Context context) {
			mContext = context;
		}

		public int getCount() {
			return actorName.size();
		}

		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(final int position, View convertView, ViewGroup parent) {

			final ViewHolder holder = new ViewHolder();

			if (convertView == null) {
				convertView = LayoutInflater.from(mContext).inflate(
						R.layout.results_list_item, null);
				convertView.setTag(holder);
			}

			holder.txtTitle = (TextView) convertView.findViewById(R.id.txtTitle);
			holder.txtTitle.setText(actorName.get(position));

			holder.txtDesc = (TextView) convertView.findViewById(R.id.txtDesc);
			holder.txtDesc.setText(characterName.get(position));

			NetworkImageView image = (NetworkImageView) convertView
					.findViewById(R.id.imgMovie);

			ImageLoader imageLoader = VolleySingleton.getInstance(getActivity())
					.getImageLoader();

			String thumbPath = actorThumb.get(position);

			image.setDefaultImageResId(R.drawable.no_image);

			if (!thumbPath.isEmpty()) {
				image.setImageUrl(thumbPath, imageLoader);
			}

			boolean isSelected = favIsSelected.get(position);

			final Drawable imgOn = getActivity().getResources().getDrawable(
					R.drawable.ic_fav_on);

			final Drawable imgOff = getActivity().getResources().getDrawable(
					R.drawable.ic_fav_off);

			holder.imgFav = (ImageView) convertView.findViewById(R.id.imgFav);

			if (isSelected) {
				holder.imgFav.setImageDrawable(imgOn);
			} else {
				holder.imgFav.setImageDrawable(imgOff);
			}

			holder.imgFav.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					String thumb = actorThumb.get(position);
					String name = actorName.get(position);

					boolean wasSelected = favIsSelected.get(position);

					if (wasSelected) {
						favIsSelected.set(position, false);

						holder.imgFav.setImageDrawable(imgOff);

						FavoriteActor.deleteFavoriteActor(getActivity(),
								String.valueOf(actorID.get(position)));
					} else {
						favIsSelected.set(position, true);

						holder.imgFav.setImageDrawable(imgOn);

						new AddFavTask().execute(actorID.get(position), name, thumb);
					}
				}
			});

			return convertView;
		}

		private Context mContext;
	}

	private class AddFavTask extends AsyncTask<Object, Void, Void> {
		@Override
		protected Void doInBackground(Object... params) {
			FavoriteActor.add(getActivity(), (Long) params[0], (String) params[1],
					(String) params[2]);
			return null;
		}
	}
}
