package com.lmccarty.trailersearch.activity;

import java.lang.reflect.Type;
import java.util.Stack;

import org.json.JSONObject;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.lmccarty.trailersearch.R;
import com.lmccarty.trailersearch.fragment.About;
import com.lmccarty.trailersearch.fragment.ApiStrings;
import com.lmccarty.trailersearch.fragment.DetailsData;
import com.lmccarty.trailersearch.fragment.DialogDeleteActorFav;
import com.lmccarty.trailersearch.fragment.DialogDeleteMovieFav;
import com.lmccarty.trailersearch.fragment.FavoriteActors;
import com.lmccarty.trailersearch.fragment.FavoriteMovies;
import com.lmccarty.trailersearch.fragment.MainList;
import com.lmccarty.trailersearch.fragment.MainList.OnRTMovieListItemClicked;
import com.lmccarty.trailersearch.fragment.MovieCredits;
import com.lmccarty.trailersearch.fragment.SearchResults;
import com.lmccarty.trailersearch.fragment.SearchResultsActor;
import com.lmccarty.trailersearch.fragment.ShowCast;
import com.lmccarty.trailersearch.fragment.FavoriteActors.OnFavActorsClicked;
import com.lmccarty.trailersearch.fragment.FavoriteMovies.OnFavMoviesClicked;
import com.lmccarty.trailersearch.model.FavoriteActor;
import com.lmccarty.trailersearch.model.FavoriteMovie;
import com.lmccarty.trailersearch.model.OnActorListClicked;
import com.lmccarty.trailersearch.model.OnMovieListClicked;
import com.lmccarty.trailersearch.model.ShowMovieCast;
import com.lmccarty.trailersearch.model.TmdbConfiguration;
import com.lmccarty.trailersearch.model.VolleySingleton;
import com.lmccarty.trailersearch.util.AppPrefs;
import com.lmccarty.trailersearch.util.UIUtils;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.ActionBar;
import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnActionExpandListener;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;

public class MainActivity extends Activity implements OnRTMovieListItemClicked,
		OnActionExpandListener, OnQueryTextListener, OnFavActorsClicked,
		OnFavMoviesClicked, ShowMovieCast, OnActorListClicked, OnMovieListClicked {

	private static final String TAG = MainActivity.class.getSimpleName();

	public static final String LIST = "main_list_frag";
	public static final String LIST_SEARCH = "search_list_frag";
	public static final String LIST_ACTOR = "actor_list_frag";
	public static final String LIST_FAV_MOVIES = "list_fav_movies";
	public static final String LIST_FAV_ACTORS = "list_fav_actors";
	public static final String LIST_SHOW_CAST = "list_show_cast";
	public static final String LIST_CREDITS = "credits_list_frag";
	public static final String LIST_TITLE = "title_list_frag";
	public static final String DETAILS = "details_frag";
	public static final String DETAILS_DATA = "details_data_frag";
	public static final String DETAILS_HELPER = "details_helper";
	public static final String DETAILS_FOR_TMDB = "details_for_tmdb";
	public static final String DETAILS_TITLE_SEARCH = "details_title_search";
	public static final String EMPTY_DATA_SET = "empty_data_set";
	public static final String VIDEO = "video_frag";
	public static final String POSTER_ART = "poster_art";
	public static final String POSTER_PATH = "poster_path";
	public static final String IMAGES = "images";
	public static final String ACTOR_ID = "actor_id";
	public static final String ACTOR_NAME = "actor_name";
	public static final String CREDITS = "credits";
	public static final String IMDB_ID = "imdb_id";
	public static final String TMDB_ID = "mdb_id";
	public static final String VIDEO_SOURCE = "video_source";
	public static final String MOVIE_CREDITS = "movie_credits";
	public static final String MOVIE_QUERY = "movie_query";
	public static final String MOVIE_RESULTS = "movie_results";
	public static final String MOVIE_ID = "movie_id";
	public static final String TOTAL_PAGES = "total_pages";
	public static final String TOTAL_RESULTS = "total_results";
	public static final String CURRENT_PAGE = "page";
	public static final String IMDB_ID_PREFIX = "tt";
	public static final String CAST = "cast";
	public static final String RESULTS = "results";
	public static final String YOUTUBE = "youtube";
	public static final String MOVIE_TITLE = "movie_title";
	public static final String MOVIE_SYNOPSIS = "movie_synopsis";
	public static final String OVERVIEW = "overview";
	public static final String SEARCH_TYPE = "search_type";
	public static final String DIALOG_DEL_FAV_ACTOR = "dialog_del_fav_actor";
	public static final String DIALOG_DEL_FAV_MOVIE = "dialog_del_fav_movie";
	public static final String DIALOG_SAVE_FAV_ACTOR = "dialog_save_fav_actor";
	public static final String MOVIE_DATA = "movie_data";

	private static final int GET_CODE = 0;
	private static final int GET_CODE_2 = 1;

	private String deleteID;

	public static String tmdbConfigUrl = null;

	private String[] drawerList;
	private DrawerLayout drawerLayout;
	private ActionBarDrawerToggle drawerToggle;
	private ListView drawerListView;

	private MenuItem titleMenuItem;
	private MenuItem actorMenuItem;

	private SearchView titleSearchView;
	private SearchView actorSearchView;

	private RequestQueue requestQueue;

	// Keep track of parent activities
	public static Stack<Class<?>> parents = new Stack<Class<?>>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		parents.push(getClass());

		requestQueue = VolleySingleton.getInstance(this).getRequestQueue();

		getConfigUrl();

		drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		drawerList = getResources().getStringArray(R.array.nav_drawer_array);
		drawerListView = (ListView) findViewById(R.id.left_drawer);
		drawerListView.setAdapter(new ArrayAdapter<String>(this, R.layout.drawer_list_item,
				drawerList));
		drawerListView.setOnItemClickListener(new DrawerItemClickListener());

		final ActionBar actionBar = getActionBar();

		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setHomeButtonEnabled(true);

		drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.drawable.ic_drawer,
				R.string.drawer_open, R.string.drawer_close) {

			public void onDrawerClosed(View view) {
				super.onDrawerClosed(view);
				actionBar.setTitle(getResources().getString(R.string.app_name));
			}

			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
				actionBar.setTitle(getResources().getString(R.string.navigation));
			}
		};

		drawerLayout.setDrawerListener(drawerToggle);

		if (savedInstanceState == null) {
			addLayoutFrags();
		}
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		drawerToggle.syncState();
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == GET_CODE) {
			if (resultCode == RESULT_CANCELED) {
				Log.i(TAG, "RESULT CANCELED: ");
			} else {
				MovieCredits fragCred = (MovieCredits) getFragmentManager()
						.findFragmentByTag(LIST_CREDITS);

				String actorName = data.getExtras().getString(ACTOR_NAME);
				long actorID = data.getExtras().getLong(ACTOR_ID);

				if (data != null) {
					if (fragCred == null || !fragCred.getClass().equals(MovieCredits.class)) {

						Bundle args = new Bundle();

						MovieCredits frag = new MovieCredits();

						args.putString(ACTOR_NAME, actorName);
						args.putLong(ACTOR_ID, actorID);
						frag.setArguments(args);

						getFragmentManager()
								.beginTransaction()
								.replace(R.id.container_main, frag,
										MainActivity.LIST_CREDITS)
								.addToBackStack(MainActivity.LIST_CREDITS).commit();
					} else {
						fragCred.updateUI(actorName, actorID);
					}
				}
			}
		}
		if (requestCode == GET_CODE_2) {
			if (resultCode == RESULT_CANCELED) {
				Log.i(TAG, "RESULT CANCELED 2: ");
			} else {

				getFragmentManager().popBackStackImmediate(MainActivity.LIST_SEARCH,
						FragmentManager.POP_BACK_STACK_INCLUSIVE);

				String actorName = data.getExtras().getString(ACTOR_NAME);
				long actorID = data.getExtras().getLong(ACTOR_ID);

				Bundle args = new Bundle();
				args.putString(ACTOR_NAME, actorName);
				args.putLong(ACTOR_ID, actorID);

				MovieCredits frag = (MovieCredits) getFragmentManager().findFragmentByTag(
						LIST_CREDITS);

				if (frag == null || !frag.getClass().equals(MovieCredits.class)) {

					MovieCredits fragCred = new MovieCredits();
					fragCred.setArguments(args);

					getFragmentManager()
							.beginTransaction()
							.replace(R.id.container_main, fragCred,
									MainActivity.LIST_CREDITS)
							.addToBackStack(MainActivity.LIST_CREDITS).commit();
				} else {
					frag.updateUI(actorName, actorID);
				}
			}
		}
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		drawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);

		titleMenuItem = menu.findItem(R.id.action_search_title);
		titleMenuItem.setOnActionExpandListener(this);

		actorMenuItem = menu.findItem(R.id.action_search_actor);
		actorMenuItem.setOnActionExpandListener(this);

		titleSearchView = (SearchView) titleMenuItem.getActionView();
		titleSearchView
				.setQueryHint(getResources().getString(R.string.action_search_title));
		titleSearchView.setOnQueryTextListener(this);

		actorSearchView = (SearchView) actorMenuItem.getActionView();
		actorSearchView
				.setQueryHint(getResources().getString(R.string.action_search_actor));
		actorSearchView.setOnQueryTextListener(this);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (drawerToggle.onOptionsItemSelected(item)) {
			return true;
		}

		switch (item.getItemId()) {
		case R.id.action_search_title:
			return false;
		case R.id.action_search_actor:
			return false;
		case R.id.action_about:
			if (UIUtils.isRightInLayout(this)) {
				About about = new About();
				getFragmentManager().beginTransaction()
						.replace(R.id.container_right, about, "about_frag").commit();
			} else {
				startActivity(new Intent(this, AboutActivity.class));
			}
			return false;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public boolean onMenuItemActionCollapse(MenuItem item) {
		return true;
	}

	@Override
	public boolean onMenuItemActionExpand(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.action_search_title:
			actorMenuItem.collapseActionView();
			break;
		case R.id.action_search_actor:
			titleMenuItem.collapseActionView();
			break;
		}
		return true;
	}

	@Override
	public boolean onQueryTextChange(String newText) {
		return true;
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
		if (!query.isEmpty()) {
			if (titleMenuItem.isActionViewExpanded()) {
				if (UIUtils.isRightInLayout(this)) {
					titleMenuItem.collapseActionView();

					SearchResults fragResults = new SearchResults();

					Bundle bundle = new Bundle();
					bundle.putString("movie_query", query);
					fragResults.setArguments(bundle);

					getFragmentManager().beginTransaction()
							.replace(R.id.container_main, fragResults, "title_list_frag")
							.commit();
				} else {
					startActivity(new Intent(this, SearchResultsTitleActivity.class)
							.putExtra("movie_query", query));
				}
			} else if (actorMenuItem.isActionViewExpanded()) {
				if (UIUtils.isRightInLayout(this)) {
					actorMenuItem.collapseActionView();

					SearchResultsActor frag = new SearchResultsActor();

					Bundle bundle = new Bundle();
					bundle.putString("movie_query", query);
					frag.setArguments(bundle);

					getFragmentManager().beginTransaction()
							.replace(R.id.container_main, frag, "actor_list_frag").commit();
				} else {
					startActivity(new Intent(this, SearchResultsActorActivity.class)
							.putExtra("movie_query", query));
				}
			}
		}
		return false;
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		finish();
	}

	// TODO collapse search box
	// TODO move this?
	/*
	@Override
	public void listLoaded() {
		if (titleMenuItem.isActionViewExpanded())
			titleMenuItem.collapseActionView();

		if (actorMenuItem.isActionViewExpanded())
			actorMenuItem.collapseActionView();
	}
	*/

	/**
	 * When the listview in DataList fragment is clicked, either an activity or
	 * a fragment is started, depending on the type of device.
	 * 
	 * @param title
	 *            , posterImage, synopsis, id a number of strings with data to
	 *            be displayed in the new fragment, or activity.
	 */
	//@Override
	// TODO
	/*
	public void listItemClicked(String... params) {
		if (UIUtils.isRightInLayout(this)) {
			Details frag = (Details) getFragmentManager().findFragmentByTag(DETAILS);
			if (frag == null || !frag.getClass().equals(Details.class)) {
				Bundle bundle = new Bundle();
				bundle.putStringArray("movie_data", params);

				Details details = new Details();
				details.setArguments(bundle);

				getFragmentManager().beginTransaction()
						.replace(R.id.container_right, details, DETAILS).commit();
			} else {
				frag.setupUI(params);
			}
		} else {
			Intent intent = new Intent(this, DetailsMainListActivity.class).putExtra("movie_data",
					params);
			startActivityForResult(intent, GET_CODE_2);
		}
	}
	*/

	@Override
	public void movieTitleClicked(String imdbID) {
		if (UIUtils.isRightInLayout(this)) {
			DetailsData frag = (DetailsData) getFragmentManager().findFragmentByTag(
					"details_data_frag");
			if (frag == null || !frag.getClass().equals(DetailsData.class)) {
				Bundle bundle = new Bundle();
				bundle.putString("imdb_id", imdbID);

				DetailsData details = new DetailsData();
				details.setArguments(bundle);

				getFragmentManager().beginTransaction()
						.replace(R.id.container_right, details, "details_data_frag")
						.commit();
			} else {
				frag.setupUI(imdbID);
			}
		} else {
			Intent intent = new Intent(this, DetailsDataActivity.class).putExtra("imdb_id",
					imdbID);
			startActivityForResult(intent, GET_CODE_2);
		}
	}

	@Override
	public void movieTitleClicked(long theMDbID) {
		if (UIUtils.isRightInLayout(this)) {
			DetailsData frag = (DetailsData) getFragmentManager().findFragmentByTag(
					"details_data_frag");
			if (frag == null || !frag.getClass().equals(DetailsData.class)) {
				Bundle bundle = new Bundle();
				bundle.putLong("the_mdb_id", theMDbID);

				DetailsData details = new DetailsData();
				details.setArguments(bundle);

				getFragmentManager().beginTransaction()
						.replace(R.id.container_right, details, "details_data_frag")
						.commit();
			} else {
				frag.setupUI(theMDbID);
			}

		} else {
			Intent intent = new Intent(this, DetailsDataActivity.class).putExtra(
					"the_mdb_id", theMDbID);
			startActivityForResult(intent, GET_CODE_2);
		}
	}

	private void addLayoutFrags() {
		Fragment fragList = getFragmentManager().findFragmentByTag(LIST);
		if (fragList == null || !fragList.getClass().equals(MainList.class)) {
			getFragmentManager().beginTransaction()
					.replace(R.id.container_main, new MainList(), LIST).commit();
		}
		if (UIUtils.isRightInLayout(this)) {
			getFragmentManager().beginTransaction()
					.replace(R.id.container_right, new EmptyDataSet(), EMPTY_DATA_SET)
					.commit();
		}
	}

	/*
	 * Get the path from The Movie Database where images for movie posters are
	 * kept
	 */
	private void getConfigUrl() {
		String url = String.format(ApiStrings.getTmdbConfigUrl(),
				ApiStrings.getMovieDbKey());

		JsonObjectRequest objRequestPath = new JsonObjectRequest(Request.Method.GET, url,
				null, new ImageResponseListener(), new ErrorListener());
		requestQueue.add(objRequestPath);
	}

	private class ImageResponseListener implements Listener<JSONObject> {
		@Override
		public void onResponse(JSONObject response) {
			JsonParser parser = new JsonParser();
			JsonObject jsonObject = parser.parse(response.toString()).getAsJsonObject();
			JsonObject images = jsonObject.get(MainActivity.IMAGES).getAsJsonObject();

			Gson gson = new Gson();

			Type configType = new TypeToken<TmdbConfiguration>() {
			}.getType();

			TmdbConfiguration config = gson.fromJson(images, configType);

			String size = config.getPosterSizes().get(1);
			String baseUrl = config.getBaseUrl();

			tmdbConfigUrl = baseUrl + size;

			String url = baseUrl + size;
			String urlPrefs = AppPrefs.getTmdbConfigUrl(MainActivity.this);

			if (!url.equals(urlPrefs)) {
				AppPrefs.setTmdbConfigUrl(MainActivity.this, url);
			}
		}
	}

	private class ErrorListener implements Response.ErrorListener {
		@Override
		public void onErrorResponse(VolleyError error) {
			Log.e(TAG, "RESPONSE ERROR: " + error.toString());
		}
	}

	private class DrawerItemClickListener implements ListView.OnItemClickListener {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
			switch (pos) {
			case 0:
				getFragmentManager().beginTransaction()
						.replace(R.id.container_main, new MainList(), LIST).commit();
				break;
			case 1:
				getFragmentManager()
						.beginTransaction()
						.replace(R.id.container_main, new FavoriteMovies(), LIST_FAV_MOVIES)
						.commit();
				break;
			case 2:
				getFragmentManager()
						.beginTransaction()
						.replace(R.id.container_main, new FavoriteActors(), LIST_FAV_ACTORS)
						.commit();
				break;
			}
			drawerListView.setItemChecked(pos, true);
			drawerLayout.closeDrawer(drawerListView);
		}
	}

	// TODO empty data set
	/*
	@Override
	public void setEmptyDataSet() {
		getFragmentManager().beginTransaction()
				.replace(R.id.container_right, new EmptyDataSet(), EMPTY_DATA_SET).commit();
	}
	*/

	// TODO remove
	private String deleteFromTable;
	private String deleteFromTable2;
	private String deleteFromCol;

	@Override
	public void showActorDialog(String actorID) {
		deleteID = actorID;

		DialogFragment newFragment = DialogDeleteActorFav
				.newInstance(R.string.delete_fav_actor);
		newFragment.show(getFragmentManager(), DIALOG_DEL_FAV_ACTOR);
	}

	@Override
	public void showMovieDialog(String theMDBID) {
		deleteID = theMDBID;

		DialogFragment newFragment = DialogDeleteMovieFav
				.newInstance(R.string.delete_fav_movie);
		newFragment.show(getFragmentManager(), DIALOG_DEL_FAV_MOVIE);
	}

	@Override
	public void showMovieDetails(long id, String aName) {
		if (UIUtils.isRightInLayout(this)) {
			getFragmentManager().popBackStackImmediate();

			MovieCredits frag = (MovieCredits) getFragmentManager().findFragmentByTag(
					MainActivity.LIST_CREDITS);

			if (frag == null || !frag.getClass().equals(MovieCredits.class)) {
				frag = new MovieCredits();
				Bundle bundle = new Bundle();
				bundle.putLong(MainActivity.ACTOR_ID, id);
				bundle.putString(MainActivity.ACTOR_NAME, aName);
				frag.setArguments(bundle);

				getFragmentManager().beginTransaction()
						.replace(R.id.container_main, frag, MainActivity.MOVIE_CREDITS)
						.commit();
			}
		} else {
			startActivity(new Intent(this, MovieCreditsActivity.class).putExtra(
					MainActivity.ACTOR_ID, id).putExtra(MainActivity.ACTOR_NAME, aName));
		}
	}

	public void deleteFavActor() {
		FavoriteActor.deleteFavoriteActor(MainActivity.this, String.valueOf(deleteID));

		resetFavActorsList();
	}

	public void deleteFavMovie() {
		Log.i(TAG, "Delete this: " + deleteID);
		FavoriteMovie.delete(MainActivity.this, Long.valueOf(deleteID));

		resetFavMoviesList();
	}

	public void doNegativeClick() {
		Log.i(TAG, "NEG CLICK");
	}

	private class DeleteFavActorTask extends AsyncTask<String, Void, Void> {

		@Override
		protected void onPostExecute(Void params) {
			resetFavActorsList();
		}

		@Override
		protected Void doInBackground(String... params) {
			String[] args = { params[2] };
			return null;
		}
	}

	private void resetFavActorsList() {
		FavoriteActors frag = (FavoriteActors) getFragmentManager().findFragmentByTag(
				LIST_FAV_ACTORS);
		frag.resetList();
	}

	private void resetFavMoviesList() {
		FavoriteMovies frag = (FavoriteMovies) getFragmentManager().findFragmentByTag(
				LIST_FAV_MOVIES);
		frag.resetList();
	}

	@Override
	public void showMovieCredits(Long actorID, String actorName) {
		getFragmentManager().popBackStackImmediate();

		MovieCredits frag = (MovieCredits) getFragmentManager().findFragmentByTag(
				MainActivity.LIST_CREDITS);

		if (frag == null || !frag.getClass().equals(MovieCredits.class)) {
			frag = new MovieCredits();
			Bundle bundle = new Bundle();
			bundle.putLong("actor_id", actorID);
			bundle.putString("actor_name", actorName);
			frag.setArguments(bundle);

			getFragmentManager().beginTransaction()
					.replace(R.id.container_main, frag, MainActivity.MOVIE_CREDITS)
					.commit();
		}
	}

	@Override
	public void showMovieCast(String imdbID, String title) {
		ShowCast cast = new ShowCast();

		Bundle bundle = new Bundle();
		bundle.putString("movie_id", imdbID);
		bundle.putString("movie_title", title);
		cast.setArguments(bundle);

		getFragmentManager().beginTransaction()
				.replace(R.id.container_main, cast, "list_show_cast").commit();
	}
}
