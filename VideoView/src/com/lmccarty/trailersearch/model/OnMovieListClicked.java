package com.lmccarty.trailersearch.model;

public interface OnMovieListClicked {
	public void movieTitleClicked(long theMDbID);
}
