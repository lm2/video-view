package com.lmccarty.trailersearch.model;

import android.content.Context;

import com.lmccarty.trailersearch.R;
import com.lmccarty.trailersearch.dao.DatabaseConstants;
import com.lmccarty.trailersearch.fragment.ApiStrings;

public class EnumDVDReleaseType {

	DVDReleaseType type;

	private static final String URL = "http://api.rottentomatoes.com/api/public/v1.0/";
	private static final String DVD_LIST = "lists/dvds";
	private static final String TOP_RENTALS = "/top_rentals";
	private static final String CURRENT_RELEASES = "/current_releases";
	private static final String NEW_RELEASES = "/new_releases";
	private static final String UPCOMING = "/upcoming";
	private static final String JSON_FORMAT = ".json?";

	private Context ctx = null;

	public EnumDVDReleaseType(Context ctx, DVDReleaseType type) {
		this.ctx = ctx;
		this.type = type;
	}

	public enum DVDReleaseType {
		TOP_RENTALS, CURRENT_RELEASES, NEW_RELEASES, UPCOMING
	}

	public String releaseURL() {
		String url = null;
		String tomatoKey = ApiStrings.getTomatoKey();

		switch (type) {
		case TOP_RENTALS:
			url = URL + DVD_LIST + TOP_RENTALS + JSON_FORMAT + tomatoKey;
			break;
		case CURRENT_RELEASES:
			url = URL + DVD_LIST + CURRENT_RELEASES + JSON_FORMAT + tomatoKey;
			break;
		case NEW_RELEASES:
			url = URL + DVD_LIST + NEW_RELEASES + JSON_FORMAT + tomatoKey;
			break;
		case UPCOMING:
			url = URL + DVD_LIST + UPCOMING + JSON_FORMAT + tomatoKey;
			break;
		}

		return url;
	}

	public String dvdReleaseListParent() {
		String releaseListParent = null;

		switch (type) {
		case TOP_RENTALS:
			releaseListParent = ctx.getResources().getString(R.string.top_rentals);
			break;
		case CURRENT_RELEASES:
			releaseListParent = ctx.getResources().getString(R.string.current_releases);
			break;
		case NEW_RELEASES:
			releaseListParent = ctx.getResources().getString(R.string.new_releases);
			break;
		case UPCOMING:
			releaseListParent = ctx.getResources().getString(R.string.upcoming);
			break;
		}

		return releaseListParent;
	}

	public String dvdReleasesTableName() {
		String table = null;

		switch (type) {
		case TOP_RENTALS:
			table = DatabaseConstants.TOP_RENTALS_TABLE;
			break;
		case CURRENT_RELEASES:
			table = DatabaseConstants.CURRENT_RELEASES_TABLE;
			break;
		case NEW_RELEASES:
			table = DatabaseConstants.NEW_RELEASES_TABLE;
			break;
		case UPCOMING:
			table = DatabaseConstants.UPCOMING_TABLE;
			break;
		}

		return table;
	}
}
