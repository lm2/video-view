package com.lmccarty.trailersearch.util;

import android.util.Log;

import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.lmccarty.trailersearch.activity.MainActivity;

public class GetImDbID {

	private GetImDbID() {
		throw new AssertionError();
	}

	private static final String TAG = GetImDbID.class.getSimpleName();

	public static String getMovieResult(String json) {
		String theMovieDbID = null;
		JsonParser parser = new JsonParser();

		try {
			JsonObject jsonObject = parser.parse(json.toString())
					.getAsJsonObject();
			theMovieDbID = jsonObject.get(MainActivity.IMDB_ID).getAsString();
		} catch (JsonParseException e) {
			Log.e(TAG, "Json ERROR: " + e.getMessage());
		}

		return theMovieDbID;
	}
}
