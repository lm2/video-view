package com.lmccarty.trailersearch.fragment;

import com.lmccarty.trailersearch.R;
import com.lmccarty.trailersearch.activity.MainActivity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

public class DialogDeleteMovieFav extends DialogFragment {

	public static DialogDeleteMovieFav newInstance(int message) {
		DialogDeleteMovieFav frag = new DialogDeleteMovieFav();
		Bundle args = new Bundle();
		args.putInt("message", message);
		frag.setArguments(args);
		return frag;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		int message = getArguments().getInt("message");

		return new AlertDialog.Builder(getActivity())
				.setMessage(message)
				.setPositiveButton(
						getActivity().getResources().getString(
								R.string.btn_remove),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								((MainActivity) getActivity()).deleteFavMovie();
							}
						})
				.setNegativeButton(
						getActivity().getResources().getString(
								R.string.btn_cancel),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								((MainActivity) getActivity())
										.doNegativeClick();
							}
						}).create();
	}
}
