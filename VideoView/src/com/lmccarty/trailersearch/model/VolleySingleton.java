package com.lmccarty.trailersearch.model;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.lmccarty.trailersearch.util.BitmapLruCache;

public class VolleySingleton {

	private static VolleySingleton instance = null;
	private RequestQueue requestQueue;
	private ImageLoader imageLoader;

	private VolleySingleton(Context context) {
		requestQueue = Volley.newRequestQueue(context);
		imageLoader = new ImageLoader(requestQueue, new BitmapLruCache());
	}

	public static synchronized VolleySingleton getInstance(Context cxt) {

		if (instance == null) {
			instance = new VolleySingleton(cxt);
		}
		return instance;
	}

	public RequestQueue getRequestQueue() {
		return this.requestQueue;
	}

	public ImageLoader getImageLoader() {
		return this.imageLoader;
	}
}
