package com.lmccarty.trailersearch.util;

import java.util.Comparator;

public class DateSort {

	private String title;
	private String date;
	private String imagePath;
	private long id;
	private boolean fav;

	public DateSort(String title, String date, String path, long id, boolean fav) {
		this.title = title;
		this.date = date;
		this.imagePath = path;
		this.id = id;
		this.fav = fav;
	}

	public static Comparator<DateSort> yearComparator = new Comparator<DateSort>() {
		@Override
		public int compare(DateSort a, DateSort b) {

			String firstYear = a.getDate();
			String secondYear = b.getDate();

			return secondYear.compareTo(firstYear);
		}
	};

	public String getTitle() {
		return title;
	}

	public String getDate() {
		return date;
	}

	public String getImagePath() {
		return imagePath;
	}

	public long getID() {
		return id;
	}

	public boolean getFav() {
		return fav;
	}
}
