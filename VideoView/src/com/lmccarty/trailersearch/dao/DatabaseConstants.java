package com.lmccarty.trailersearch.dao;

public interface DatabaseConstants {
	 String THE_MDB_ACTOR_FAV = "the_mdb_actor_fav";
	 String ACTOR_ID = "actor_id";
	 String ACTOR_NAME = "actor_name";
	 String MOVIE_FAV = "movie_fav";
	 String THE_MDB_ID = "the_mdb_id";
	 String IMDB_ID = "imdb_id";
	 String RT_ID = "rt_id";
	 String THUMB = "thumb";
	 String TITLE = "title";
	 String RT_MOVIE_FAV = "rt_movie_fav";
	 String ALTERNATE_IDS = "alternate_ids";
	 String THE_MDB_MOVIE_FAV = "the_mdb_movie_fav";

	 String TOP_RENTALS_TABLE = "top_rentals";
	 String CURRENT_RELEASES_TABLE = "current_releases";
	 String NEW_RELEASES_TABLE = "new_releases";
	 String UPCOMING_TABLE = "upcoming";
	
	 String YEAR = "year";
	 String CRITICS_CONSENSUS = "critics_consensus";
	 String THUMB_PATH = "thumb_path";
	 String POSTER_PATH = "poster_path";
	 String SYNOPSIS = "synopsis";
	 String ALTERNATE_ID = "alternate_id";
	 String TOMATO_ID = "tomato_id";
}
