package com.lmccarty.trailersearch.model;

/**
 * The results when a user searches for a movie title
 */
public class ActorResults {

	int page;
	int total_pages;
	int total_results;

	private boolean adult;
	private long id;
	private String name;
	private double popularity;
	private String profile_path;

	public void setAdult(boolean b) {
		adult = b;
	}

	public boolean getAdult() {
		return adult;
	}

	public void setID(long lid) {
		id = lid;
	}

	public long getID() {
		return id;
	}

	public void setName(String n) {
		name = n;
	}

	public String getName() {
		return name;
	}

	public void setPopularity(double pop) {
		popularity = pop;
	}

	public double getPopularity() {
		return popularity;
	}

	public void setProfilePath(String path) {
		profile_path = path;
	}

	public String getProfilePath() {
		return profile_path;
	}
}