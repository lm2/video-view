package com.lmccarty.trailersearch.activity;

import com.lmccarty.trailersearch.fragment.DetailsData;

import com.lmccarty.trailersearch.model.ShowMovieCast;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

public class DetailsDataActivity extends Activity implements ShowMovieCast {

	private static final String TAG = DetailsDataActivity.class.getSimpleName();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Log.i(TAG, "New activity started");

		getActionBar().setDisplayHomeAsUpEnabled(true);
		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		String imdbID = extras.getString("imdb_id", null);
		long theMDbID = extras.getLong("the_mdb_id", 0L);

		placeFragmentInLayout(imdbID, theMDbID);
	}

	@Override
	public void onNewIntent(Intent intent) {
		super.onNewIntent(intent);

		Bundle extras = intent.getExtras();
		String imdbID = extras.getString("imdb_id", null);
		long theMDbID = extras.getLong("the_mdb_id", 0L);

		placeFragmentInLayout(imdbID, theMDbID);
	}

	private void placeFragmentInLayout(String imdbID, long theMDbID) {
		DetailsData fragDetails = (DetailsData) getFragmentManager().findFragmentByTag(
				"details_data_frag");

		FragmentManager manager = getFragmentManager();
		FragmentTransaction trans = manager.beginTransaction();

		// Make sure the fragment is not created twice
		if (fragDetails == null || !fragDetails.getClass().equals(DetailsData.class)) {
			DetailsData frag = new DetailsData();
			Bundle bundle = new Bundle();

			bundle.putString("imdb_id", imdbID);
			bundle.putLong("the_mdb_id", theMDbID);

			frag.setArguments(bundle);

			trans.replace(android.R.id.content, frag, "details_data_frag").commit();
		} 
		/*
		else {
			Log.i(TAG, "Fragment already exists");
			if (imdbID != null) {
				//fragDetails.setupUI(imdbID);
			} else if (theMDbID > 0) {
				//fragDetails.setupUI(theMDbID);
			}
		}
		*/
	}

	@Override
	public void showMovieCast(String imdbID, String title) {
		startActivity(new Intent(this, ShowCastActivity.class).putExtra("movie_id", imdbID)
				.putExtra("movie_title", title));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			Intent parentActivityIntent = new Intent(this, MainActivity.parents.pop());
			parentActivityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(parentActivityIntent);
			finish();

			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
