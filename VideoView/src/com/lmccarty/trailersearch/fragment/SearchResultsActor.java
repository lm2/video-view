package com.lmccarty.trailersearch.fragment;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.lmccarty.trailersearch.R;
import com.lmccarty.trailersearch.activity.MainActivity;
import com.lmccarty.trailersearch.model.ActorResults;
import com.lmccarty.trailersearch.model.FavoriteActor;
import com.lmccarty.trailersearch.model.OnActorListClicked;
import com.lmccarty.trailersearch.model.TmdbConfiguration;
import com.lmccarty.trailersearch.model.VolleySingleton;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class SearchResultsActor extends Fragment implements OnItemClickListener {

	private static final String TAG = SearchResultsActor.class.getSimpleName();

	private String query;
	private String imageBaseUrl;

	private static final String QUERY = "query";
	private static final String BASE_URL = "base_url";

	private List<Long> actorID = new ArrayList<Long>();
	private List<String> name = new ArrayList<String>();
	private List<String> profilePath = new ArrayList<String>();

	private List<Boolean> favIsSelected;

	private RequestQueue requestQueue;
	private ImageLoader imageLoader;

	private ProgressBar progress;
	private TextView txtEmpty;
	private ListView list;
	private ListAdapter adapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestQueue = VolleySingleton.getInstance(getActivity()).getRequestQueue();
		adapter = new ActorListAdapter(getActivity());

		if (savedInstanceState != null) {
			query = savedInstanceState.getString(QUERY);
			imageBaseUrl = savedInstanceState.getString(BASE_URL);
		} else {
			Bundle args = this.getArguments();
			query = args.getString(MainActivity.MOVIE_QUERY);
		}

		getActivity().getActionBar().setSubtitle(query);
	}

	// Interface
	OnActorListClicked mCallback;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mCallback = (OnActorListClicked) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnActorListClicked");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.list_title, container, false);

		list = (ListView) view.findViewById(R.id.list);
		list.setOnItemClickListener(this);

		progress = (ProgressBar) view.findViewById(R.id.progList);
		txtEmpty = (TextView) view.findViewById(R.id.txtEmpty);

		return view;
	}

	@Override
	public void onStart() {
		super.onStart();
		getData();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		requestQueue.cancelAll(this);
	}

	@Override
	public void onSaveInstanceState(Bundle save) {
		save.putString(QUERY, query);
		save.putString(BASE_URL, imageBaseUrl);
	}

	private void getData() {
		String url = String.format(ApiStrings.getTmdbConfigUrl(),
				ApiStrings.getMovieDbKey());

		JsonObjectRequest objRequestPath = new JsonObjectRequest(Request.Method.GET, url,
				null, new ImageResponseListener(), new ErrorListener());
		requestQueue.add(objRequestPath);

		JsonObjectRequest objRequestActor = new JsonObjectRequest(Request.Method.GET,
				createActorUrl(), null, new ActorResponseListener(), new ErrorListener());
		requestQueue.add(objRequestActor);
	}

	private class ImageResponseListener implements Listener<JSONObject> {
		@Override
		public void onResponse(JSONObject response) {
			JsonParser parser = new JsonParser();

			try {
				JsonObject jsonObject = parser.parse(response.toString()).getAsJsonObject();
				JsonObject images = jsonObject.get(MainActivity.IMAGES).getAsJsonObject();

				Gson gson = new Gson();

				Type configType = new TypeToken<TmdbConfiguration>() {
				}.getType();

				TmdbConfiguration lConfig = gson.fromJson(images, configType);

				String size = lConfig.getPosterSizes().get(1);
				String sBaseUrl = lConfig.getBaseUrl();
				imageBaseUrl = sBaseUrl + size;
			} catch (JsonParseException e) {
				Log.e(TAG, "Json ERROR: " + e.getMessage());
			}
		}
	}

	private class ActorResponseListener implements Listener<JSONObject> {
		@Override
		public void onResponse(JSONObject response) {
			JsonParser parser = new JsonParser();

			try {
				JsonObject jsonObject = parser.parse(response.toString()).getAsJsonObject();
				JsonArray actors = jsonObject.get(MainActivity.RESULTS).getAsJsonArray();

				Gson gson = new Gson();

				Type type = new TypeToken<List<ActorResults>>() {
				}.getType();

				List<ActorResults> actor = gson.fromJson(actors, type);

				int size = actors.size();
				List<Long> idList = new ArrayList<Long>(size);
				List<String> nameList = new ArrayList<String>(size);
				List<String> imageProfilePath = new ArrayList<String>(size);
				List<Double> popularityList = new ArrayList<Double>(size);
				List<Boolean> selected = new ArrayList<Boolean>(size);

				for (ActorResults data : actor) {
					long aID = data.getID();
					String dataName = data.getName();
					String dataProfile = data.getProfilePath();

					double dataPopularity = data.getPopularity();

					idList.add(aID);
					nameList.add(dataName);
					imageProfilePath.add(dataProfile != null ? imageBaseUrl + dataProfile
							: "");
					popularityList.add(dataPopularity);

					selected.add(FavoriteActor.isFavoriteActor(getActivity(), aID));
				}

				actorID = idList;
				name = nameList;
				profilePath = imageProfilePath;
				favIsSelected = selected;

				if (nameList.size() == 0) {
					txtEmpty.setVisibility(View.VISIBLE);
				} else {
					if (txtEmpty.getVisibility() == View.VISIBLE)
						txtEmpty.setVisibility(View.GONE);
				}
				list.setAdapter(adapter);

				progress.setVisibility(View.INVISIBLE);
			} catch (JsonParseException e) {
				Log.e(TAG, "Json ERROR: " + e.getMessage());
			}
		}
	}

	private class ErrorListener implements Response.ErrorListener {
		@Override
		public void onErrorResponse(VolleyError error) {
			Log.e(TAG, "RESPONSE ERROR: " + error.toString());
		}
	}

	public void doSearch(String sQuery) {
		progress.setVisibility(View.VISIBLE);
		query = sQuery;

		getData();
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		mCallback.showMovieCredits(actorID.get(arg2), name.get(arg2));
	}

	private String createActorUrl() {
		String query = null;
		String url = ApiStrings.getActorDbUrl();
		String newUrl = null;
		try {
			query = URLEncoder.encode(this.query, "utf-8");
			newUrl = String.format(url, ApiStrings.getMovieDbKey(), query);
		} catch (UnsupportedEncodingException e) {
			Log.e(TAG, e.getMessage());
		}
		return newUrl;
	}

	private class ActorListAdapter extends BaseAdapter {
		public ActorListAdapter(Context context) {
			mContext = context;
		}

		public int getCount() {
			return name.size();
		}

		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(final int position, View convertView, ViewGroup parent) {

			if (convertView == null) {
				convertView = LayoutInflater.from(mContext).inflate(
						R.layout.results_list_item, null);
			}

			TextView txtTitle = (TextView) convertView.findViewById(R.id.txtTitle);
			txtTitle.setText(name.get(position));

			final ImageView imgFav = (ImageView) convertView.findViewById(R.id.imgFav);

			NetworkImageView image = (NetworkImageView) convertView
					.findViewById(R.id.imgMovie);
			imageLoader = VolleySingleton.getInstance(getActivity()).getImageLoader();
			image.setDefaultImageResId(R.drawable.no_profile);
			image.setImageUrl(profilePath.get(position), imageLoader);

			boolean isSelected = favIsSelected.get(position);

			final Drawable imgOn = getActivity().getResources().getDrawable(
					R.drawable.ic_fav_on);

			final Drawable imgOff = getActivity().getResources().getDrawable(
					R.drawable.ic_fav_off);

			if (isSelected) {
				imgFav.setImageDrawable(imgOn);
			} else {
				imgFav.setImageDrawable(imgOff);
			}

			imgFav.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					boolean wasSelected = favIsSelected.get(position);

					if (wasSelected) {
						favIsSelected.set(position, false);

						imgFav.setImageDrawable(imgOff);

						FavoriteActor.deleteFavoriteActor(getActivity(),
								String.valueOf(actorID.get(position)));

					} else {
						favIsSelected.set(position, true);

						imgFav.setImageDrawable(imgOn);

						new AddFavTask().execute(actorID.get(position), name.get(position),
								profilePath.get(position));
					}
				}
			});

			return convertView;
		}

		private Context mContext;
	}

	private class AddFavTask extends AsyncTask<Object, Void, Void> {
		@Override
		protected Void doInBackground(Object... params) {
			FavoriteActor.add(getActivity(), (Long) params[0], (String) params[1],
					(String) params[2]);
			return null;
		}
	}
}
