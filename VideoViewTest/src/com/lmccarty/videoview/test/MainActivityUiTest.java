package com.lmccarty.videoview.test;

import android.view.KeyEvent;

import com.android.uiautomator.core.UiDevice;
import com.android.uiautomator.core.UiObject;
import com.android.uiautomator.core.UiObjectNotFoundException;
import com.android.uiautomator.core.UiSelector;
import com.android.uiautomator.testrunner.UiAutomatorTestCase;

public class MainActivityUiTest extends UiAutomatorTestCase {

	private static final String ACTOR = "pitt";
	private static final String MOVIE_TITLE = "go";
	private static final String OPTIONS = "More options";
	private static final String SEARCH_ACTOR = "Search actor";
	private static final String SEARCH_QUERY = "Search query";
	private static final String SEARCH_TITLE = "Search title";

	private UiDevice device;

	@Override
	public void setUp() throws Exception {
		super.setUp();
		device = getUiDevice();
	}

	public void testTitleSearch() throws UiObjectNotFoundException {

		// Test title search
		UiObject menuButton = new UiObject(new UiSelector().className(
				"android.widget.ImageButton").description(OPTIONS));
		menuButton.click();

		UiObject menuTitle = new UiObject(new UiSelector().className(
				"android.widget.TextView").text(SEARCH_TITLE));
		menuTitle.click();

		UiObject editTitleSearch = new UiObject(new UiSelector().className(
				"android.widget.EditText").description(SEARCH_QUERY));

		editTitleSearch.setText(MOVIE_TITLE);

		assertTrue(editTitleSearch.getText().toString().equals(MOVIE_TITLE));

		getUiDevice().pressKeyCode(KeyEvent.KEYCODE_ENTER);

		UiObject backButton = new UiObject(new UiSelector().className(
				"android.widget.ImageView").index(0));
		backButton.click();

		// Test actor search
		menuButton.click();
		UiObject menuActor = new UiObject(new UiSelector().className(
				"android.widget.TextView").text(SEARCH_ACTOR));
		menuActor.click();

		UiObject editActorSearch = new UiObject(new UiSelector().className(
				"android.widget.EditText").description(SEARCH_QUERY));
		editActorSearch.setText(ACTOR);

		assertTrue(editActorSearch.getText().toString().equals(ACTOR));

		getUiDevice().pressKeyCode(KeyEvent.KEYCODE_ENTER);
		backButton.click();

		int x = device.getDisplayWidth();
		int y = device.getDisplayHeight() / 2;
		device.swipe(0, y, x, y, 100);

		assertTrue(backButton.click());
	}
}
