package com.lmccarty.trailersearch.activity;

public abstract class DataStrings {

	private DataStrings() {
	}

	private static long actorID;
	private static String actorName;
	private static String movieTitle;

	private static String[] dataArray;

	public static void setActorName(String name) {
		actorName = name;
	}

	public static String getActorName() {
		return actorName;
	}

	public static void setActorID(long id) {
		actorID = id;
	}

	public static long getActorID() {
		return actorID;
	}

	public static void setMovieTitle(String title) {
		movieTitle = title;
	}

	public static String getMovieTitle() {
		return movieTitle;
	}

	public static void setDataArray(String[] data) {
		dataArray = data;
	}

	public static String[] getDataArray() {
		return dataArray;
	}
}
