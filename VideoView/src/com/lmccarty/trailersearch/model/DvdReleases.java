package com.lmccarty.trailersearch.model;

import java.util.List;
import java.util.Map;
import android.content.ContentValues;
import android.content.Context;
import android.util.Log;

import com.lmccarty.trailersearch.dao.DAOFactory;
import com.lmccarty.trailersearch.dao.DAOHelper;
import com.lmccarty.trailersearch.dao.DvdReleasesDAO;
import com.lmccarty.trailersearch.dao.FavoriteActorDAO;

public class DvdReleases {

	private static DAOFactory daoFactory = DAOFactory.getInstance();

	private List<String> title = null;
	private List<Integer> year = null;
	private List<String> thumb = null;
	private List<String> imdbID = null;
	private List<Long> tomatoID = null;
	private List<String> critics = null;
	private List<String> poster = null;
	private List<String> synopsis = null;

	public DvdReleases(List<String> title, List<Integer> year, List<String> thumb,
			List<String> imdbID, List<Long> tomatoID, List<String> critics,
			List<String> poster, List<String> synopsis) {
		this.title = title;
		this.year = year;
		this.thumb = thumb;
		this.imdbID = imdbID;
		this.tomatoID = tomatoID;
		this.critics = critics;
		this.poster = poster;
		this.synopsis = synopsis;
	}

	public static void add(Context ctx, List<Movie> movie, String table) {
		DvdReleasesDAO dao = null;
		try {
			dao = daoFactory.getDvdReleasesDAO(ctx);
			dao.addDvdReleases(movie, table);
		} finally {
			dao.close();
		}
	}

	public static DvdReleases getDvdReleases(Context ctx, String table) {
		DvdReleasesDAO dao = null;
		try {
			dao = daoFactory.getDvdReleasesDAO(ctx);
			return dao.getDvdReleases(ctx, table);
		} finally {
			dao.close();
		}
	}
	
	public static void deleteAllRows(Context ctx, String table){
		DvdReleasesDAO dao = null;
		try {
			dao = daoFactory.getDvdReleasesDAO(ctx);
			dao.deleteAllRows(table);
		} finally {
			dao.close();
		}
	}

	public List<String> getTitleList() {
		return title;
	}

	public List<Integer> getYearList() {
		return year;
	}

	public List<String> getThumbList() {
		return thumb;
	}

	public List<String> getImdbIDList() {
		return imdbID;
	}

	public List<Long> getTomatoIDList() {
		return tomatoID;
	}

	public List<String> getCriticsList() {
		return critics;
	}

	public List<String> getPosterList() {
		return poster;
	}

	public List<String> getSynopsisList() {
		return synopsis;
	}
}
