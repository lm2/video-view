package com.lmccarty.trailersearch.fragment;

import com.lmccarty.trailersearch.R;

import android.app.Fragment;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class About extends Fragment {

	private static final String TAG = About.class.getSimpleName();

	private TextView txtVersion;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.about, container, false);

		txtVersion = (TextView) view.findViewById(R.id.txtVersion);
		txtVersion.setText(getVersion());

		return view;
	}

	private String getVersion() {
		String version = "1.0";
		try {
			PackageInfo info = getActivity().getPackageManager()
					.getPackageInfo(getActivity().getPackageName(), 0);
			version = info.versionName;
		} catch (NameNotFoundException e) {
			Log.e(TAG, "ERROR: " + e.getMessage());
		}
		return version;
	}
}
