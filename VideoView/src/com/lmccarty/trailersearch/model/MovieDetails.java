package com.lmccarty.trailersearch.model;

import android.annotation.SuppressLint;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class MovieDetails {

	// Required parameters
	private final String title;
	private final long id;
	private final String imdbID;

	// Optional parameters
	private final String posterPath;
	private final String overview;
	private final Map<Integer, String> genres;

	public static class Builder {

		private final String title;
		private final long theMDbID;
		private final String imdbID;

		private String posterPath = null;
		private String overview = null;
		@SuppressLint("UseSparseArrays")
		private Map<Integer, String> genres = new HashMap<Integer, String>();

		public Builder(String title, long theMDbID, String imdbID) {
			this.title = title;
			this.theMDbID = theMDbID;
			this.imdbID = imdbID;
		}

		public Builder posterPath(String path) {
			posterPath = path;
			return this;
		}

		public Builder overview(String ov) {
			overview = ov;
			return this;
		}

		public Builder genres(Map<Integer, String> map) {
			genres = map;
			return this;
		}

		public MovieDetails build() {
			return new MovieDetails(this);
		}
	}

	private MovieDetails(Builder builder) {
		title = builder.title;
		id = builder.theMDbID;
		imdbID = builder.imdbID;
		posterPath = builder.posterPath;
		overview = builder.overview;
		genres = builder.genres;
	}

	public String getTitle() {
		return title;
	}

	public String getIMDbID() {
		return imdbID;
	}

	public long getTheMDbID() {
		return id;
	}

	public String getPosterPath() {
		return posterPath;
	}

	public String getOverview() {
		return overview;
	}

	public Map<Integer, String> getGenres() {
		return genres;
	}

	@Override
	public String toString() {
		return "Title: " + title + " ID: " + id + " IMDB ID: " + imdbID + " Poster path: "
				+ posterPath + " Overview: " + overview + " Genres: " + genres.toString();
	}
}
