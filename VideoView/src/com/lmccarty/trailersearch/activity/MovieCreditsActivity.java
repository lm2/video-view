package com.lmccarty.trailersearch.activity;

import com.lmccarty.trailersearch.fragment.MovieCredits;
import com.lmccarty.trailersearch.model.OnMovieListClicked;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

public class MovieCreditsActivity extends Activity implements OnMovieListClicked {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		MainActivity.parents.push(getClass());

		placeFragmentInLayout(getIntent().getExtras());
	}

	@Override
	public void onNewIntent(Intent intent) {
		placeFragmentInLayout(intent.getExtras());

		MovieCredits fragCred = (MovieCredits) getFragmentManager().findFragmentByTag(
				"movie_credits");
		fragCred.clearListOnNewIntent();
	}

	private void placeFragmentInLayout(Bundle extras) {
		long id = 0;
		String name = null;
		if (extras != null) {
			// TODO refactor
			id = extras.getLong("actor_id");
			name = extras.getString("actor_name");
			DataStrings.setActorName(name);
			DataStrings.setActorID(id);
		} else {
			// Returning from details
			id = DataStrings.getActorID();
			name = DataStrings.getActorName();
		}

		FragmentManager manager = getFragmentManager();
		FragmentTransaction trans = manager.beginTransaction();

		MovieCredits fragCred = (MovieCredits) getFragmentManager().findFragmentByTag(
				"movie_credits");

		// Make sure that the frag is not created twice
		if (fragCred == null || !fragCred.getClass().equals(MovieCredits.class)) {
			MovieCredits frag = new MovieCredits();
			Bundle bundle = new Bundle();
			bundle.putLong("actor_id", id);
			bundle.putString("actor_name", name);
			frag.setArguments(bundle);

			trans.replace(android.R.id.content, frag, "movie_credits").commit();
		} else {
			fragCred.updateUI(name, id);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void movieTitleClicked(long theMDbID) {
		Intent intent = new Intent(this, DetailsDataActivity.class).putExtra("the_mdb_id",
				theMDbID);
		startActivity(intent);
	}
}
