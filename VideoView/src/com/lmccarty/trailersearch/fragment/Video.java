package com.lmccarty.trailersearch.fragment;

import com.lmccarty.trailersearch.R;
import com.lmccarty.trailersearch.activity.MainActivity;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class Video extends Fragment {

	private String videoSource = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// queue = Volley.newRequestQueue(getActivity());

		if (savedInstanceState != null) {
			// trailerName = savedInstanceState.getStringArrayList(MOVIE_NAMES);
			// trailerSource = savedInstanceState
			// .getStringArrayList(MOVIE_SOURCES);
		}

		if (getArguments() != null) {
			Bundle bundle = this.getArguments();
			String source = bundle.getString(MainActivity.VIDEO_SOURCE);
			videoSource = source;
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.video,
				container, false);

		return view;
	}

	@Override
	public void onStart() {
		super.onStart();

		Log.i("TAG", "VIDEO SOURCE: " + videoSource);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

	}

	@Override
	public void onDestroy() {
		super.onDestroy();

	}
}