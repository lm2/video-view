package com.lmccarty.trailersearch.dao;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import static android.provider.BaseColumns._ID;

public abstract class DAOHelper extends SQLiteOpenHelper implements DatabaseConstants {

	protected static final int DATABASE_VERSION = 1;
	protected static final String DATABASE_NAME = "moviedb.db";

	private static final String RT_MOVIE_TABLE_CREATE = "CREATE TABLE " + RT_MOVIE_FAV + " (" + _ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT," + RT_ID + " TEXT, " + IMDB_ID
			+ " TEXT UNIQUE, " + THE_MDB_ID + ", " + TITLE + " TEXT," + THUMB + " TEXT)";

	private static final String TMDB_MOVIE_TABLE_CREATE = "CREATE TABLE " + THE_MDB_MOVIE_FAV
			+ " (" + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + THE_MDB_ID + " TEXT UNIQUE,"
			+ IMDB_ID + " TEXT, " + TITLE + " TEXT, " + THUMB + " TEXT)";

	private static final String TMDB_FAV_ACTOR_TABLE_CREATE = "CREATE TABLE " + THE_MDB_ACTOR_FAV
			+ " (" + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + ACTOR_ID + " UNIQUE, "
			+ ACTOR_NAME + ", " + THUMB + ")";

	public DAOHelper(Context context, String name, CursorFactory factory, int version) {
		super(context, name, factory, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(RT_MOVIE_TABLE_CREATE);
		db.execSQL(TMDB_MOVIE_TABLE_CREATE);
		db.execSQL(TMDB_FAV_ACTOR_TABLE_CREATE);

		db.execSQL("CREATE TABLE " + TOP_RENTALS_TABLE + "(" + _ID
				+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + TITLE + " TEXT, " + YEAR + " INTEGER, "
				+ CRITICS_CONSENSUS + " TEXT, " + THUMB_PATH + " TEXT, " + POSTER_PATH + " TEXT, "
				+ SYNOPSIS + " TEXT, " + ALTERNATE_ID + " TEXT UNIQUE, " + TOMATO_ID
				+ " INTEGER UNIQUE );");

		db.execSQL("CREATE TABLE " + CURRENT_RELEASES_TABLE + "(" + _ID
				+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + TITLE + " TEXT, " + YEAR + " INTEGER, "
				+ THUMB_PATH + " TEXT, " + CRITICS_CONSENSUS + " TEXT, " + POSTER_PATH + " TEXT, "
				+ SYNOPSIS + " TEXT, " + ALTERNATE_ID + " TEXT UNIQUE, " + TOMATO_ID
				+ " INTEGER UNIQUE );");

		db.execSQL("CREATE TABLE " + NEW_RELEASES_TABLE + "(" + _ID
				+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + TITLE + " TEXT, " + YEAR + " INTEGER, "
				+ THUMB_PATH + " TEXT, " + CRITICS_CONSENSUS + " TEXT, " + POSTER_PATH + " TEXT, "
				+ SYNOPSIS + " TEXT, " + ALTERNATE_ID + " TEXT UNIQUE, " + TOMATO_ID
				+ " INTEGER UNIQUE );");

		db.execSQL("CREATE TABLE " + UPCOMING_TABLE + "(" + _ID
				+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + TITLE + " TEXT, " + YEAR + " INTEGER, "
				+ THUMB_PATH + " TEXT, " + CRITICS_CONSENSUS + " TEXT, " + POSTER_PATH + " TEXT, "
				+ SYNOPSIS + " TEXT, " + ALTERNATE_ID + " TEXT UNIQUE, " + TOMATO_ID
				+ " INTEGER UNIQUE );");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		//db.execSQL();
		//db.execSQL();
		//db.execSQL();
		//onCreate(db);
	}

	@Override
	public void onConfigure(SQLiteDatabase db) {
		super.onOpen(db);
		if (!db.isReadOnly()) {
			// Enable foreign key constraints
			db.execSQL("PRAGMA foreign_keys = ON;");
		}
	}

	protected void closeCursor(Cursor cursor) {
		if (cursor != null) {
			cursor.close();
		}
	}
}
