package com.lmccarty.trailersearch.dao;
import android.content.Context;

public class DAOFactory {

	private static DAOFactory instance = null;

	public static DAOFactory getInstance() {
		if (instance == null) {
			instance = new DAOFactory();
		}
		return instance;
	}

	public DAOFactory() {
	}

	public FavoriteActorDAO getFavoriteActorDAO(Context ctx) {
		return new FavoriteActorDAO(ctx);
	}

	public FavoriteMovieDAO getFavoriteMovieDAO(Context ctx) {
		return new FavoriteMovieDAO(ctx);
	}

	public DvdReleasesDAO getDvdReleasesDAO(Context ctx) {
		return new DvdReleasesDAO(ctx);
	}
}
