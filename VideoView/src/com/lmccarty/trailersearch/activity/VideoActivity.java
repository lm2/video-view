package com.lmccarty.trailersearch.activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.lmccarty.trailersearch.R;
import com.lmccarty.trailersearch.fragment.ApiStrings;

public class VideoActivity extends YouTubeFailureRecoveryActivity implements
		CompoundButton.OnCheckedChangeListener,
		YouTubePlayer.OnFullscreenListener {

	private static final int PORTRAIT_ORIENTATION = Build.VERSION.SDK_INT < 9 ? ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
			: ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT;

	// private LinearLayout baseLayout;
	private YouTubePlayerView playerView;
	private YouTubePlayer player;

	private boolean fullscreen = true;

	private String resource;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.video);

		// baseLayout = (LinearLayout) findViewById(R.id.layout);
		playerView = (YouTubePlayerView) findViewById(R.id.player);

		playerView.initialize(ApiStrings.getYoutubeKey(), this);

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		resource = extras.getString(MainActivity.VIDEO_SOURCE);

		doLayout();
	}

	@Override
	public void onInitializationSuccess(YouTubePlayer.Provider provider,
			YouTubePlayer player, boolean wasRestored) {
		this.player = player;

		// Specify that we want to handle fullscreen behavior ourselves.
		player.addFullscreenControlFlag(YouTubePlayer.FULLSCREEN_FLAG_CUSTOM_LAYOUT);
		player.setOnFullscreenListener(this);
		if (!wasRestored) {
			// player.cueVideo(resource.isEmpty() ? "avP5d16wEp0" : resource);
			player.loadVideo(resource.isEmpty() ? "avP5d16wEp0" : resource);
		}
	}

	@Override
	protected YouTubePlayer.Provider getYouTubePlayerProvider() {
		return playerView;
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		int controlFlags = player.getFullscreenControlFlags();
		if (isChecked) {
			// If you use the FULLSCREEN_FLAG_ALWAYS_FULLSCREEN_IN_LANDSCAPE,
			// your activity's normal UI
			// should never be laid out in landscape mode (since the video will
			// be fullscreen whenever the
			// activity is in landscape orientation). Therefore you should set
			// the activity's requested
			// orientation to portrait. Typically you would do this in your
			// AndroidManifest.xml, we do it
			// programmatically here since this activity demos fullscreen
			// behavior both with and without
			// this flag).
			setRequestedOrientation(PORTRAIT_ORIENTATION);
			controlFlags |= YouTubePlayer.FULLSCREEN_FLAG_ALWAYS_FULLSCREEN_IN_LANDSCAPE;
		} else {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
			controlFlags &= ~YouTubePlayer.FULLSCREEN_FLAG_ALWAYS_FULLSCREEN_IN_LANDSCAPE;
		}
		player.setFullscreenControlFlags(controlFlags);
	}

	private void doLayout() {
		LinearLayout.LayoutParams playerParams = (LinearLayout.LayoutParams) playerView
				.getLayoutParams();
		if (fullscreen) {
			// When in fullscreen, the visibility of all other views than the
			// player should be set to
			// GONE and the player should be laid out across the whole screen.
			playerParams.width = LayoutParams.MATCH_PARENT;
			playerParams.height = LayoutParams.MATCH_PARENT;
		}
	}

	@Override
	public void onFullscreen(boolean isFullscreen) {
		fullscreen = isFullscreen;
		doLayout();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		doLayout();
	}
}
