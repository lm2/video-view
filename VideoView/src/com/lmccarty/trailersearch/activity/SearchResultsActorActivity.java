package com.lmccarty.trailersearch.activity;

import com.lmccarty.trailersearch.R;
import com.lmccarty.trailersearch.fragment.SearchResultsActor;
import com.lmccarty.trailersearch.model.OnActorListClicked;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;

public class SearchResultsActorActivity extends Activity implements OnActorListClicked,
		OnQueryTextListener {

	private MenuItem menuItemTitle;
	private MenuItem menuItemActor;
	private SearchView searchViewActor;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		String query = null;
		if (extras != null) {
			query = extras.getString(MainActivity.MOVIE_QUERY);
			DataStrings.setActorName(query);
		} else {
			// Returning from child activity
			query = DataStrings.getActorName();
		}

		FragmentManager manager = getFragmentManager();
		FragmentTransaction trans = manager.beginTransaction();

		SearchResultsActor frag = new SearchResultsActor();

		Bundle bundle = new Bundle();
		bundle.putString(MainActivity.MOVIE_QUERY, query);
		frag.setArguments(bundle);

		trans.replace(android.R.id.content, frag, MainActivity.LIST_ACTOR).commit();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);

		menuItemTitle = menu.findItem(R.id.action_search_title);
		menuItemTitle.setVisible(false);

		menuItemActor = menu.findItem(R.id.action_search_actor);
		menuItemActor.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		menuItemActor.setVisible(true);

		searchViewActor = (SearchView) menuItemActor.getActionView();
		searchViewActor
				.setQueryHint(getResources().getString(R.string.action_search_actor));
		searchViewActor.setOnQueryTextListener(this);

		return true;
	}

	@Override
	public boolean onQueryTextChange(String arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
		getActionBar().setSubtitle(query);
		searchViewActor.onActionViewCollapsed();
		SearchResultsActor frag = (SearchResultsActor) getFragmentManager()
				.findFragmentByTag(MainActivity.LIST_ACTOR);

		frag.doSearch(query);
		return false;
	}

	@Override
	public void showMovieCredits(Long actorID, String actorName) {
		startActivity(new Intent(this, MovieCreditsActivity.class).putExtra("actor_id",
				actorID).putExtra("actor_name", actorName));
	}
}
