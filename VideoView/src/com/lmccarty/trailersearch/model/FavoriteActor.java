package com.lmccarty.trailersearch.model;

import java.util.ArrayList;
import java.util.List;
import android.content.Context;
import com.lmccarty.trailersearch.dao.DAOFactory;
import com.lmccarty.trailersearch.dao.FavoriteActorDAO;

public class FavoriteActor {

	private long actorID;
	private String actorName;
	private String thumb;

	private static DAOFactory daoFactory = DAOFactory.getInstance();

	public FavoriteActor(long actorID, String name, String thumb) {
		this.actorID = actorID;
		this.actorName = name;
		this.thumb = thumb;
	}

	public Long getActorID() {
		return actorID;
	}

	public String getName() {
		return actorName;
	}

	public String getThumb() {
		return thumb;
	}

	public static void add(Context ctx, long aID, String name, String path) {
		FavoriteActorDAO dao = null;

		try {
			dao = daoFactory.getFavoriteActorDAO(ctx);
			dao.addFavoriteActor(new FavoriteActor(aID, name, path));
		} finally {
			if (dao != null)
				dao.close();
		}
	}

	public static boolean isFavoriteActor(Context ctx, long aID) {
		FavoriteActorDAO dao = null;
		boolean favorite = false;
		try {
			dao = daoFactory.getFavoriteActorDAO(ctx);
			favorite = dao.isFavoriteActor(aID);
		} finally {
			if (dao != null)
				dao.close();
		}
		return favorite;
	}

	public static void deleteFavoriteActor(Context ctx, String id) {
		FavoriteActorDAO dao = null;
		try {
			dao = daoFactory.getFavoriteActorDAO(ctx);
			dao.deleteFavoriteActor(id);
		} finally {
			if (dao != null)
				dao.close();
		}
	}

	public static List<List<String>> getFavoriteActorsList(Context ctx) {
		FavoriteActorDAO dao = null;
		List<List<String>> actors = new ArrayList<List<String>>();
		try {
			actors = daoFactory.getFavoriteActorDAO(ctx).getFavoriteActors();
		} finally {
			if (dao != null) {
				dao.close();
			}
		}

		return actors;
	}
}
