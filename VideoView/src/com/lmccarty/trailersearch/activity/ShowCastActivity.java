package com.lmccarty.trailersearch.activity;

import com.lmccarty.trailersearch.fragment.ShowCast;
import com.lmccarty.trailersearch.model.OnActorListClicked;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.MenuItem;

public class ShowCastActivity extends Activity implements OnActorListClicked {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getActionBar().setDisplayHomeAsUpEnabled(true);

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		String id = extras.getString(MainActivity.MOVIE_ID);
		String title = extras.getString(MainActivity.MOVIE_TITLE);

		FragmentManager manager = getFragmentManager();
		FragmentTransaction trans = manager.beginTransaction();

		ShowCast frag = new ShowCast();
		Bundle bundle = new Bundle();
		bundle.putString(MainActivity.MOVIE_ID, id);
		bundle.putString(MainActivity.MOVIE_TITLE, title);
		frag.setArguments(bundle);

		trans.replace(android.R.id.content, frag, MainActivity.LIST_SHOW_CAST).commit();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// Respond to the action bar's Up/Home button
		case android.R.id.home:
			//	NavUtils.navigateUpFromSameTask(this);
			//	return true;
			finish();
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void showMovieCredits(Long actorID, String actorName) {
		startActivity(new Intent(this, MovieCreditsActivity.class).putExtra("actor_id",
				actorID).putExtra("actor_name", actorName));
	}
}
