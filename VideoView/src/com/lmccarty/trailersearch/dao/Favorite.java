package com.lmccarty.trailersearch.dao;

import com.lmccarty.trailersearch.model.FavoriteActor;

public interface Favorite {

	//void add();

	void delete();

	FavoriteActor add(FavoriteActor actor);
}
