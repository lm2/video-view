package com.lmccarty.trailersearch.util;

import android.content.Context;
import android.content.SharedPreferences;

public class AppPrefs {

	private static final String PREFS = "movie_search_prefs";
	private static final String CONFIG_URL = "tmdb_config_url";
	private static final String DL_DVD_RELEASE_AGE = "dl_dvd_release_age";

	private AppPrefs() {
		throw new AssertionError();
	}

	public String getPrefsFileName() {
		return PREFS;
	}

	public static void setTmdbConfigUrl(Context ctx, String config) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFS, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(CONFIG_URL, config);
		editor.commit();
	}

	public static String getTmdbConfigUrl(Context ctx) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFS, 0);
		return settings.getString(CONFIG_URL, "");
	}

	public static long getDownloadedDvdReleaseAge(Context ctx) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFS, 0);
		return settings.getLong(DL_DVD_RELEASE_AGE, 0L);
	}

	public static void setLastDownloadTime(Context ctx) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFS, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putLong(DL_DVD_RELEASE_AGE, System.currentTimeMillis());
		editor.commit();
	}
}
