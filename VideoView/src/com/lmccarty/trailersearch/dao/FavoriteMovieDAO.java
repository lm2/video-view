package com.lmccarty.trailersearch.dao;

import static android.provider.BaseColumns._ID;

import java.util.ArrayList;
import java.util.List;

import com.lmccarty.trailersearch.model.FavoriteMovie;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

public class FavoriteMovieDAO extends DAOHelper {

	private static final String TAG = FavoriteMovieDAO.class.getSimpleName();

	public FavoriteMovieDAO(Context ctx) {
		super(ctx, DATABASE_NAME, null, DATABASE_VERSION);
	}

	public boolean isFavoriteMovie(String id) {
		boolean fav = false;
		Cursor cursor = null;
		String sql = null;
		int count = 0;

		try {
			SQLiteDatabase db = getReadableDatabase();
			sql = "SELECT " + _ID + " FROM " + THE_MDB_MOVIE_FAV + " WHERE " + THE_MDB_ID
					+ " = ?";
			String[] args = { id };

			cursor = db.rawQuery(sql, args);
			count = cursor.getCount();
			if (count > 0) {
				fav = true;
			} else {
				sql = "SELECT " + _ID + " FROM " + RT_MOVIE_FAV + " WHERE " + THE_MDB_ID
						+ " = ?";
				cursor = db.rawQuery(sql, args);
				count = cursor.getCount();
				if (count > 0) {
					fav = true;
				}
			}
		} finally {
			closeCursor(cursor);
		}

		return fav;
	}

	/**
	 * Check RT, and The MDB for favorites return true if the movie has been
	 * saved as a favorite
	 * 
	 * @param the
	 *            IMDB movie ID
	 * @return boolean
	 */
	public boolean isFavoriteMovieCheckIMDB(String id) {
		boolean fav = false;
		Cursor cursor = null;
		String sql = null;
		int count = 0;

		try {
			SQLiteDatabase db = getReadableDatabase();
			sql = "SELECT " + _ID + " FROM " + THE_MDB_MOVIE_FAV + " WHERE " + IMDB_ID
					+ " = ?";
			String[] args = { id };

			cursor = db.rawQuery(sql, args);
			count = cursor.getCount();
			if (count > 0) {
				fav = true;
			} else {
				sql = "SELECT " + _ID + " FROM " + RT_MOVIE_FAV + " WHERE " + IMDB_ID
						+ " = ?";
				cursor = db.rawQuery(sql, args);
				count = cursor.getCount();
				if (count > 0) {
					fav = true;
				}
			}
		} finally {
			closeCursor(cursor);
		}

		return fav;
	}

	public void addFavoriteMovie(FavoriteMovie movie) {
		long id = 0;
		try {
			SQLiteDatabase db = getWritableDatabase();
			ContentValues values = new ContentValues();
			values.put(IMDB_ID, movie.getIMDBID());
			values.put(RT_ID, movie.getRTID());
			values.put(THE_MDB_ID, movie.getTheMDBID());
			values.put(TITLE, movie.getTitle());
			values.put(THUMB, movie.getThumb());
			id = db.insertOrThrow(RT_MOVIE_FAV, null, values);
		} catch (SQLiteException e) {
			Log.d(TAG, "Could not insert int db: " + e.getMessage());
		}

		if (id > 0) {
			Log.i(TAG, "New favorite movie saved: " + movie.getTitle());
		} else {
			Log.d(TAG,
					"Could not save title: " + movie.getTitle() + " IMDB ID: "
							+ movie.getIMDBID() + " RT ID: " + movie.getRTID());
		}
	}

	public void addFavoriteTheMDBMovie(FavoriteMovie movie) {
		long id = 0;
		try {
			SQLiteDatabase db = getWritableDatabase();
			ContentValues values = new ContentValues();
			values.put(IMDB_ID, movie.getIMDBID());
			values.put(THE_MDB_ID, movie.getTheMDBID());
			values.put(TITLE, movie.getTitle());
			values.put(THUMB, movie.getThumb());
			id = db.insertOrThrow(THE_MDB_MOVIE_FAV, null, values);
		} catch (SQLiteException e) {
			Log.d(TAG, "Could not insert int db: " + e.getMessage());
		}

		if (id > 0) {
			Log.i(TAG, "New favorite movie saved: " + movie.getTitle());
		}
	}

	public void deleteFavoriteMovie(FavoriteMovie movie) {
		SQLiteDatabase db = getWritableDatabase();

		db.delete(THE_MDB_MOVIE_FAV, IMDB_ID + " = ?", new String[] { movie.getIMDBID() });
		db.delete(RT_MOVIE_FAV, RT_ID + " = ?",
				new String[] { String.valueOf(movie.getRTID()) });
		db.delete(RT_MOVIE_FAV, IMDB_ID + " = ?", new String[] { movie.getIMDBID() });
	}

	public void deleteFavoriteMovieFavsList(FavoriteMovie movie) {
		SQLiteDatabase db = getWritableDatabase();

		db.delete(THE_MDB_MOVIE_FAV, THE_MDB_ID + " = " + movie.getTheMDBID(), null);
		db.delete(RT_MOVIE_FAV, THE_MDB_ID + " = " + movie.getTheMDBID(), null);
	}

	public List<List<Object>> getFavoriteMovieList() {

		List<List<Object>> l = null;
		Cursor cursor = null;
		try {
			SQLiteDatabase db = getWritableDatabase();

			String sql = "SELECT " + TITLE + "," + THUMB + "," + IMDB_ID + "," + THE_MDB_ID
					+ " FROM " + THE_MDB_MOVIE_FAV + " UNION SELECT " + TITLE + "," + THUMB
					+ "," + IMDB_ID + "," + THE_MDB_ID + " FROM " + RT_MOVIE_FAV
					+ " ORDER BY " + TITLE + " COLLATE NOCASE ASC";

			cursor = db.rawQuery(sql, null);

			List<Object> title = new ArrayList<Object>();
			List<Object> path = new ArrayList<Object>();
			List<Object> id = new ArrayList<Object>();

			while (cursor.moveToNext()) {
				title.add(cursor.getString(0));
				path.add(cursor.getString(1));
				id.add(cursor.getLong(3));
			}

			l = new ArrayList<List<Object>>();
			l.add(title);
			l.add(path);
			l.add(id);
		} finally {
			closeCursor(cursor);
		}

		return l;
	}
}
