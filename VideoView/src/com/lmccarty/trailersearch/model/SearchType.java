package com.lmccarty.trailersearch.model;
public class SearchType {

	Search search;

	public enum Search {
		TITLE, ACTOR
	}

	public SearchType(Search search) {
		this.search = search;
	}
}
