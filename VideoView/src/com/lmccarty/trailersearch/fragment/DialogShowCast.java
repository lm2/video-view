package com.lmccarty.trailersearch.fragment;

import java.util.List;
import com.lmccarty.trailersearch.R;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

public class DialogShowCast extends DialogFragment implements
		OnItemClickListener {

	//private static List<String> names = new ArrayList<String>();
	//private static List<String> ids = new ArrayList<String>();

	public static DialogShowCast newInstance(List<String> actorName,
			List<String> actorID) {
		DialogShowCast frag = new DialogShowCast();

		//names = actorName;
		//ids = actorID;
		// Bundle args = new Bundle();
		// args.putStringArrayList(NAME, (ArrayList<String>) actorName);
		// args.putStringArrayList(ID, (ArrayList<String>) actorID);
		// frag.setArguments(args);
		return frag;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// int message = getArguments().getInt("message");
		// List<String> names = getArguments().getStringArrayList(NAME);
		// List<String> ids = getArguments().getStringArrayList(ID);

		View view = getActivity().getLayoutInflater().inflate(
				R.layout.list_title, null);

		return new AlertDialog.Builder(getActivity())
				// .setMessage(message)
				.setView(view)
				.setNegativeButton(
						getActivity().getResources().getString(
								R.string.btn_close),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								// List closed
							}
						}).create();
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub

	}
}