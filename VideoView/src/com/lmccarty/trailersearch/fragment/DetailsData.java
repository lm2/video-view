package com.lmccarty.trailersearch.fragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.lmccarty.trailersearch.R;
import com.lmccarty.trailersearch.activity.MainActivity;
import com.lmccarty.trailersearch.activity.VideoActivity;
import com.lmccarty.trailersearch.model.MovieDetails;
import com.lmccarty.trailersearch.model.ShowMovieCast;
import com.lmccarty.trailersearch.model.VolleySingleton;
import com.lmccarty.trailersearch.model.YouTube;
import com.lmccarty.trailersearch.util.JSONHelper;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class DetailsData extends Fragment implements OnClickListener {

	private static final String TAG = DetailsData.class.getSimpleName();
	private RequestQueue requestQueue;
	private RelativeLayout rlViews;
	private ProgressBar progress;
	private NetworkImageView image;
	private Button btnTrailer;
	private Button btnCast;
	private TextView txtTitle;
	private TextView txtGenre;
	private TextView txtSynopsis;
	private TextView txtAttribution;
	private String movieTitle = null;
	private String imdbID = null;
	private long theMDbID = 0L;

	private List<String> trailerSource = new ArrayList<String>();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestQueue = VolleySingleton.getInstance(getActivity()).getRequestQueue();

		Bundle bundle = this.getArguments();

		imdbID = bundle.getString("imdb_id");
		theMDbID = bundle.getLong("the_mdb_id");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.panel_details, container, false);

		progress = (ProgressBar) view.findViewById(R.id.progress);
		rlViews = (RelativeLayout) view.findViewById(R.id.rlViews);

		image = (NetworkImageView) view.findViewById(R.id.imgPosterArt);

		btnTrailer = (Button) view.findViewById(R.id.btnTrailer);
		btnTrailer.setEnabled(false);
		btnTrailer.setOnClickListener(this);

		btnCast = (Button) view.findViewById(R.id.btnCast);
		btnCast.setOnClickListener(this);

		txtTitle = (TextView) view.findViewById(R.id.txtTitle);
		txtGenre = (TextView) view.findViewById(R.id.txtGenre);
		txtSynopsis = (TextView) view.findViewById(R.id.txtSynopsis);
		txtAttribution = (TextView) view.findViewById(R.id.txtAttribution);

		return view;
	}

	@Override
	public void onResume() {
		super.onResume();
		getMovieDetails();
	}

	// interface
	ShowMovieCast mCallback;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mCallback = (ShowMovieCast) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " " + TAG
					+ " must implement ShowMovieCast");
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnTrailer:
			startActivity(new Intent(getActivity(), VideoActivity.class).putExtra(
					MainActivity.VIDEO_SOURCE, trailerSource.get(0)));// Use first trailer for now
			break;
		case R.id.btnCast:
			mCallback.showMovieCast(imdbID, movieTitle);
			break;
		}
	}

	private void getMovieDetails() {
		JsonObjectRequest objRequest = new JsonObjectRequest(Request.Method.GET,
				getDetailsUrl(), null, new DetailsResponseListener(), new ErrorListener());
		requestQueue.add(objRequest);
	}

	private class ErrorListener implements Response.ErrorListener {
		@Override
		public void onErrorResponse(VolleyError error) {
			Log.e(TAG, "Error: " + error.toString());
		}
	}

	private class TrailersResponseListener implements Listener<JSONObject> {
		@Override
		public void onResponse(JSONObject response) {
			List<YouTube> trailerData = JSONHelper.getTrailers(response);
			List<String> source = new ArrayList<String>();
			for (YouTube data : trailerData) {
				source.add(data.getSource());
			}
			if (trailerSource.size() > 0)
				trailerSource.clear();

			trailerSource.addAll(source);
			prepareTrailerButton();
		}
	}

	private class DetailsResponseListener implements Listener<JSONObject> {
		@Override
		public void onResponse(JSONObject response) {
			//MovieDetails
			MovieDetails details = JSONHelper.parseMovieDetails(getActivity(), response);
			setupUI(details);
			getTrailerData();
		}
	}

	private void setupUI(MovieDetails details) {
		txtTitle.setText(details.getTitle());
		txtSynopsis.setText(details.getOverview());
		txtAttribution.setText(getActivity().getResources()
				.getString(R.string.tmdb_details));

		if (details.getGenres().size() > 0) {
			if (txtGenre.getVisibility() != View.VISIBLE) {
				txtGenre.setVisibility(View.VISIBLE);
			}
			txtGenre.setText(getActivity().getResources().getString(R.string.genre)
					+ createGenresString(details.getGenres()));
		}

		ImageLoader imageLoader = VolleySingleton.getInstance(getActivity())
				.getImageLoader();
		image.setDefaultImageResId(R.drawable.no_image);

		image.setImageUrl(details.getPosterPath(), imageLoader);

		movieTitle = details.getTitle();
		imdbID = details.getIMDbID();

		if (rlViews.getVisibility() != View.VISIBLE)
			rlViews.setVisibility(View.VISIBLE);

		if (progress.getVisibility() == View.VISIBLE)
			progress.setVisibility(View.GONE);
	}

	public void setupUI(String imdbID) {
		showProgress();
		this.imdbID = null;
		this.imdbID = imdbID;
		this.theMDbID = 0L;

		getMovieDetails();
	}

	public void setupUI(long theMDbID) {
		showProgress();

		this.imdbID = null;
		this.theMDbID = 0L;
		this.theMDbID = theMDbID;

		getMovieDetails();
	}

	private void prepareTrailerButton() {
		if (trailerSource.size() > 0) {
			btnTrailer.setEnabled(true);
		} else {
			btnTrailer.setEnabled(false);
		}
	}

	private String createGenresString(Map<Integer, String> g) {
		StringBuilder builder = new StringBuilder();

		for (Map.Entry<Integer, String> entry : g.entrySet()) {
			if (builder.length() != 0)
				builder.append(", ");

			builder.append(entry.getValue());
		}

		return builder.toString();
	}

	private void showProgress() {
		if (progress.getVisibility() != View.VISIBLE)
			progress.setVisibility(View.VISIBLE);
	}

	private String getTrailerUrl() {
		return String.format(ApiStrings.getMovieDbTrailerUrl(), imdbID,
				ApiStrings.getMovieDbKey());
	}

	private String getDetailsUrl() {
		String url = null;
		if (imdbID != null) {
			url = String.format(ApiStrings.getSingleMovie(), imdbID,
					ApiStrings.getMovieDbKey());
		} else {
			if (theMDbID > 0) {
				url = String.format(ApiStrings.getSingleMovie(), String.valueOf(theMDbID),
						ApiStrings.getMovieDbKey());
			}
		}

		Log.i(TAG, "Details URL: " + url);
		return url;
	}

	private void getTrailerData() {
		JsonObjectRequest objRequestTrailers = new JsonObjectRequest(Request.Method.GET,
				getTrailerUrl(), null, new TrailersResponseListener(), new ErrorListener());
		requestQueue.add(objRequestTrailers);
	}
}
