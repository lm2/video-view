package com.lmccarty.trailersearch.fragment;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.lmccarty.trailersearch.R;
import com.lmccarty.trailersearch.activity.MainActivity;
import com.lmccarty.trailersearch.activity.VideoActivity;
import com.lmccarty.trailersearch.model.VolleySingleton;
import com.lmccarty.trailersearch.model.YouTube;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class DetailsSearch extends Fragment implements OnClickListener {

	private final static String TAG = DetailsSearch.class.getSimpleName();

	private final static String POSTER_PATH = "poster_path";

	private RequestQueue requestQueue;

	private String title;
	private String posterPath;
	private String movieID;

	private RelativeLayout rlViews;
	private ProgressBar progress;

	private NetworkImageView image;
	private Button btnTrailer;
	private Button btnCast;
	private TextView txtTitle;
	private TextView txtSynopsis;

	private List<String> trailerSource = new ArrayList<String>();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestQueue = VolleySingleton.getInstance(getActivity()).getRequestQueue();

		if (savedInstanceState != null) {
			movieID = savedInstanceState.getString("id");
			title = savedInstanceState.getString("title");
			posterPath = savedInstanceState.getString("path");
		} else {
			Bundle bundle = this.getArguments();
			movieID = bundle.getString(MainActivity.MOVIE_ID);
			title = bundle.getString(MainActivity.MOVIE_TITLE);
			posterPath = bundle.getString(MainActivity.POSTER_PATH);
		}

		getData();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.panel_details, container, false);

		progress = (ProgressBar) view.findViewById(R.id.progress);
		rlViews = (RelativeLayout) view.findViewById(R.id.rlViews);
		image = (NetworkImageView) view.findViewById(R.id.imgPosterArt);

		btnTrailer = (Button) view.findViewById(R.id.btnTrailer);
		btnTrailer.setOnClickListener(this);

		btnCast = (Button) view.findViewById(R.id.btnCast);
		btnCast.setOnClickListener(this);

		txtTitle = (TextView) view.findViewById(R.id.txtTitle);
		txtSynopsis = (TextView) view.findViewById(R.id.txtSynopsis);

		return view;
	}

	ShowEmptyDataSet mCallback;

	// TODO change the name of this callback to something more appropriate
	public interface ShowEmptyDataSet {
		public void setEmptyDataSet();

		public void showCast(String resource, String title);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mCallback = (ShowEmptyDataSet) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement ShowEmptyDataSet");
		}
	}

	@Override
	public void onSaveInstanceState(Bundle out) {
		out.putString("id", movieID);
		out.putString("title", title);
		out.putString("path", posterPath);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		requestQueue.cancelAll(getActivity());
	}

	private void getData() {
		String tmdbKey = ApiStrings.getMovieDbKey();

		String url = String.format(ApiStrings.getSingleMovie(), String.valueOf(movieID),
				tmdbKey);

		JsonObjectRequest objRequestCast = new JsonObjectRequest(Request.Method.GET, url,
				null, new ResponseListener(), new ErrorListener());
		requestQueue.add(objRequestCast);
	}

	private class ResponseListener implements Listener<JSONObject> {
		@Override
		public void onResponse(JSONObject response) {
			JsonParser parser = new JsonParser();
			JsonObject jsonObject = parser.parse(response.toString()).getAsJsonObject();

			String movieID = jsonObject.has(MainActivity.IMDB_ID) ? jsonObject.get(
					MainActivity.IMDB_ID).getAsString() : "";
			String overview = jsonObject.has(MainActivity.OVERVIEW)
					&& !jsonObject.get(MainActivity.OVERVIEW).isJsonNull() ? jsonObject
					.get(MainActivity.OVERVIEW).getAsString() : "";

			if (!movieID.isEmpty()) {
				getTrailerData(movieID);

				try {

					if (progress.getVisibility() != View.GONE)
						progress.setVisibility(View.GONE);

					ImageLoader imageLoader = VolleySingleton.getInstance(getActivity())
							.getImageLoader();
					image.setDefaultImageResId(R.drawable.no_image);

					if (jsonObject.has(POSTER_PATH) && MainActivity.tmdbConfigUrl != null) {
						image.setImageUrl(posterPath, imageLoader);
					}

					txtTitle.setText(title);
					txtSynopsis.setText(overview);

					if (rlViews.getVisibility() != View.VISIBLE)
						rlViews.setVisibility(View.VISIBLE);

				} catch (JsonSyntaxException e) {
					Log.e(TAG, "ERROR: " + e.getMessage());
				} catch (UnsupportedOperationException e) {
					Log.d(TAG, "JSON PARSE ERROR: " + e.getMessage());
				}
			} else {
				Toast.makeText(getActivity(), getResources().getString(R.string.no_data),
						Toast.LENGTH_SHORT).show();
				if (progress.getVisibility() != View.GONE)
					progress.setVisibility(View.GONE);

				if (rlViews.getVisibility() == View.VISIBLE)
					rlViews.setVisibility(View.GONE);

				mCallback.setEmptyDataSet();
			}
		}
	}

	private class TrailerResponseListener implements Listener<JSONObject> {
		@Override
		public void onResponse(JSONObject response) {
			parseJSON(response);
		}
	}

	private class ErrorListener implements Response.ErrorListener {
		@Override
		public void onErrorResponse(VolleyError error) {
			Log.e(TAG, "RESPONSE ERROR: " + error.toString());
		}
	}

	private void getTrailerData(String id) {
		String tmdbKey = ApiStrings.getMovieDbKey();
		String url = String.format(ApiStrings.getMovieDbTrailerUrl(), id, tmdbKey);

		JsonObjectRequest objRequest = new JsonObjectRequest(Request.Method.GET, url, null,
				new TrailerResponseListener() {
				}, new ErrorListener());
		requestQueue.add(objRequest);
	}

	private void parseJSON(JSONObject response) {
		JsonParser parser = new JsonParser();
		JsonObject jsonObject = parser.parse(response.toString()).getAsJsonObject();
		JsonArray trailers = jsonObject.get(MainActivity.YOUTUBE).getAsJsonArray();

		Gson gson = new Gson();

		Type trailerType = new TypeToken<List<YouTube>>() {
		}.getType();
		List<YouTube> trailer = gson.fromJson(trailers, trailerType);

		int size = trailer.size();

		List<String> listSource = new ArrayList<String>(size);

		for (int i = 0; i < size; i++) {
			listSource.add(trailer.get(i).getSource());
		}
		trailerSource = listSource;
		if (listSource.size() > 0) {
			btnTrailer.setEnabled(true);
		} else {
			btnTrailer.setEnabled(false);
		}
	}

	public void setupUI(String... params) {
		String id = params[0];
		String sTitle = params[1];
		String sPoster = params[2];

		movieID = id;
		title = sTitle;
		posterPath = sPoster;

		getData();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnTrailer:
			String source = trailerSource.get(0);
			startActivity(new Intent(getActivity(), VideoActivity.class).putExtra(
					MainActivity.VIDEO_SOURCE, source));
			break;
		case R.id.btnCast:
			mCallback.showCast(movieID, title);
			break;
		}
	}
}
