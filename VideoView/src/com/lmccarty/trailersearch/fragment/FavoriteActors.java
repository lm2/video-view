package com.lmccarty.trailersearch.fragment;

import java.util.ArrayList;
import java.util.List;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.lmccarty.trailersearch.R;
import com.lmccarty.trailersearch.model.FavoriteActor;
import com.lmccarty.trailersearch.model.VolleySingleton;
import android.app.Activity;
import android.app.ListFragment;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.AsyncTask.Status;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class FavoriteActors extends ListFragment implements
		OnItemLongClickListener {

	private RequestQueue queue;
	private ImageLoader imageLoader;

	private BaseAdapter adapter;
	private GetFavsTask mGetFavsTask;

	private List<String> actorID = new ArrayList<String>();
	private List<String> actorName = new ArrayList<String>();
	private List<String> thumbs = new ArrayList<String>();

	private TextView txtEmpty;

	private int longPressedIndex;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		queue = VolleySingleton.getInstance(getActivity()).getRequestQueue();

		adapter = new ActorListAdapter(getActivity());

		getActivity().getActionBar().setSubtitle(
				getActivity().getResources().getString(R.string.fav_actors));
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.panel_main, container, false);

		txtEmpty = (TextView) view.findViewById(R.id.txtEmpty);

		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		getListView().setLongClickable(true);
		getListView().setOnItemLongClickListener(FavoriteActors.this);
	}

	OnFavActorsClicked mCallback;

	public interface OnFavActorsClicked {
		public void showMovieDetails(long id, String aName);

		public void showActorDialog(String data);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mCallback = (OnFavActorsClicked) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnFavActorsClicked");
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		mGetFavsTask = (GetFavsTask) new GetFavsTask().execute();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		queue.cancelAll(this);
	}

	@Override
	public void onStop() {
		super.onStop();
		mGetFavsTask.getStatus();
		if (mGetFavsTask != null && mGetFavsTask.getStatus() == Status.RUNNING) {
			mGetFavsTask.cancel(true);
			mGetFavsTask = null;
		}
	}

	public void resetList() {

		actorID.remove(longPressedIndex);
		actorName.remove(longPressedIndex);
		thumbs.remove(longPressedIndex);

		adapter.notifyDataSetChanged();

		if (adapter.getCount() == 0)
			txtEmpty.setVisibility(View.VISIBLE);
	}

	static class ViewHolder {
		TextView txtTitle;
		// NetworkImageView image;
		ImageView imgFav;
	}

	private class ActorListAdapter extends BaseAdapter {
		public ActorListAdapter(Context context) {
			mContext = context;
		}

		public int getCount() {
			return actorName.size();
		}

		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(final int position, View convertView,
				ViewGroup parent) {

			if (convertView == null) {
				convertView = LayoutInflater.from(mContext).inflate(
						R.layout.favs_list_item, null);
			}

			final ViewHolder holder = new ViewHolder();

			holder.txtTitle = (TextView) convertView
					.findViewById(R.id.txtTitle);
			holder.txtTitle.setText(actorName.get(position));

			NetworkImageView image = (NetworkImageView) convertView
					.findViewById(R.id.imgMovie);
			imageLoader = VolleySingleton.getInstance(getActivity())
					.getImageLoader();

			String sImage = thumbs.get(position);

			image.setDefaultImageResId(R.drawable.no_image);
			image.setImageUrl(sImage, imageLoader);

			return convertView;
		}

		private Context mContext;
	}

	private class GetFavsTask extends AsyncTask<Void, Void, List<List<String>>> {
		@Override
		protected void onPostExecute(List<List<String>> param) {

			actorName = param.get(0);
			actorID = param.get(1);
			thumbs = param.get(2);

			getListView().setAdapter(adapter);
			adapter.notifyDataSetChanged();
			if (adapter.getCount() == 0)
				txtEmpty.setVisibility(View.VISIBLE);
		}

		@Override
		protected List<List<String>> doInBackground(Void... arg0) {
			return FavoriteActor.getFavoriteActorsList(getActivity());
		}
	}

	@Override
	public void onListItemClick(ListView l, View v, int pos, long id) {
		super.onListItemClick(l, v, pos, id);
		mCallback.showMovieDetails(Long.parseLong(actorID.get(pos)), actorName.get(pos));
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		longPressedIndex = arg2;
		mCallback.showActorDialog(actorID.get(arg2));
		return true;
	}
}
