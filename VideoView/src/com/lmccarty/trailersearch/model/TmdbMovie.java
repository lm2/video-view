package com.lmccarty.trailersearch.model;

public class TmdbMovie {

	private boolean adult;
	private String backdrop_path;
	// private String belongs_to_collection;
	private int budget;
	private String homepage;
	private String id;
	private String imdb_id;
	private String original_title;
	private String overview;
	private double popularity;
	private String poster_path;
	private String release_date;
	private int revenue;
	private int runtime;
	private String status;
	private String tagline;
	private String title;
	private double vote_average;
	private int vote_count;

	private Genres genres;
	private ProductionCompanies production_companies;
	private ProductionCountries production_countries;
	private SpokenLanguage spoken_language;

	private Cast[] cast;

	public TmdbMovie() {
	}

	public void setAdult(boolean a) {
		this.adult = a;
	}

	public boolean getAdult() {
		return adult;
	}

	public void setBackdropPath(String bd) {
		this.backdrop_path = bd;
	}

	public String getBackdropPath() {
		return backdrop_path;
	}

	public void setBudget(int b) {
		this.budget = b;
	}

	public int getBudget() {
		return budget;
	}

	public void setHomepage(String home) {
		this.homepage = home;
	}

	public String getHomepage() {
		return homepage;
	}

	public void setID(String sid) {
		this.id = sid;
	}

	public String getID() {
		return id;
	}

	public void setImdbId(String id) {
		this.imdb_id = id;
	}

	public String getImdbId() {
		return imdb_id;
	}

	public void setOriginalTitle(String ogTitle) {
		this.original_title = ogTitle;
	}

	public String getOriginalTitle() {
		return original_title;
	}

	public void setOverview(String over) {
		this.overview = over;
	}

	public String getOverview() {
		return overview;
	}

	public void setPopularity(double pop) {
		this.popularity = pop;
	}

	public double getPopularity() {
		return popularity;
	}

	public void setPosterPath(String poster) {
		this.poster_path = poster;
	}

	public String getPosterPath() {
		return poster_path;
	}

	public void setReleaseDate(String date) {
		this.release_date = date;
	}

	public String releaseDate() {
		return release_date;
	}

	public void setRevenue(int rev) {
		this.revenue = rev;
	}

	public int getRevenue() {
		return revenue;
	}

	public void setRuntime(int time) {
		this.runtime = time;
	}

	public int getRuntime() {
		return runtime;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	public void setTagline(String tag) {
		this.tagline = tag;
	}

	public String getTagline() {
		return tagline;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	public void setVoteAverage(double vote) {
		this.vote_average = vote;
	}

	public double getVoteAverage() {
		return vote_average;
	}

	public void setVoteCount(int count) {
		this.vote_count = count;
	}

	public int getVoteCount() {
		return vote_count;
	}

	public void setGenres(Genres genres) {
		this.genres = genres;
	}

	public Genres getGenres() {
		return genres;
	}

	public void setProductionCompanies(ProductionCompanies prod) {
		this.production_companies = prod;
	}

	public ProductionCompanies getProductionCompanies() {
		return production_companies;
	}

	public void setProductionCountries(ProductionCountries prod) {
		this.production_countries = prod;
	}

	public ProductionCountries getProductionCountries() {
		return production_countries;
	}

	public void setSpokenLanguage(SpokenLanguage lang) {
		this.spoken_language = lang;
	}

	public SpokenLanguage getSpokenLanguage() {
		return spoken_language;
	}

	public class Genres {
		private int id;
		private String name;

		public void setId(int id) {
			this.id = id;
		}

		public int getId() {
			return id;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}
	}

	public class ProductionCompanies {
		private int id;
		private String name;

		public void setId(int id) {
			this.id = id;
		}

		public int getId() {
			return id;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}
	}

	public class ProductionCountries {
		private String iso_3166_1;
		private String name;

		public void setIso(String id) {
			this.iso_3166_1 = id;
		}

		public String getIso() {
			return iso_3166_1;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}
	}

	public class SpokenLanguage {
		private String iso_639_1;
		private String name;

		public void setIso(String id) {
			this.iso_639_1 = id;
		}

		public String getIso() {
			return iso_639_1;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}
	}

	public void setCast(Cast[] cast) {
		this.cast = cast;
	}

	public Cast[] getCast() {
		return cast;
	}

	public class Cast {

		public Cast() {
		}

		private String name;
		private String id;
		private String profile_path;
		private String character;
		private String cast_id;

		public void setName(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}

		public void setID(String id) {
			this.id = id;
		}

		public String getID() {
			return id;
		}

		public void setPath(String path) {
			this.profile_path = path;
		}

		public String getPath() {
			return profile_path;
		}

		public void setCharacter(String character) {
			this.character = character;
		}

		public String getCharacter() {
			return character;
		}

		public void setCastID(String id) {
			this.cast_id = id;
		}

		public String getCastID() {
			return cast_id;
		}
	}
}