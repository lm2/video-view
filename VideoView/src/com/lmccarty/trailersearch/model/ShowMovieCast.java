package com.lmccarty.trailersearch.model;

public interface ShowMovieCast {
	public void showMovieCast(String imdbID, String title);
}
