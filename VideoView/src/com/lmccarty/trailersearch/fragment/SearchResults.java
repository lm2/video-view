package com.lmccarty.trailersearch.fragment;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.json.JSONObject;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.lmccarty.trailersearch.R;
import com.lmccarty.trailersearch.activity.MainActivity;
import com.lmccarty.trailersearch.model.FavoriteMovie;
import com.lmccarty.trailersearch.model.OnMovieListClicked;
import com.lmccarty.trailersearch.model.Results;
import com.lmccarty.trailersearch.model.TmdbConfiguration;
import com.lmccarty.trailersearch.model.VolleySingleton;
import com.lmccarty.trailersearch.util.DateSort;
import com.lmccarty.trailersearch.util.GetImDbID;
import com.lmccarty.trailersearch.util.ReadStream;
import com.lmccarty.trailersearch.util.StringFormatter;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class SearchResults extends Fragment implements OnClickListener, OnItemClickListener {

	private static final String TAG = SearchResults.class.getSimpleName();

	private String query;
	private String imageBaseUrl;
	private int currentPage = 1;
	private int totalPages = 0;
	private List<String> title = new ArrayList<String>(0);
	private List<String> releaseDate = new ArrayList<String>(0); // private
	private List<String> posterPath = new ArrayList<String>(0);
	private List<Long> movieID = new ArrayList<Long>(0);
	private List<Boolean> favIsSelected = new ArrayList<Boolean>(0);

	private TextView txtEmpty;
	private ProgressBar progress;
	private ListView list;
	private BaseAdapter adapter;

	private LinearLayout footer;
	private Button btnPrev;
	private Button btnNext;

	private RequestQueue requestQueue;
	private ImageLoader imageLoader;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestQueue = VolleySingleton.getInstance(getActivity()).getRequestQueue();
		adapter = new ResultsListAdapter(getActivity());

		if (savedInstanceState != null) {
			query = savedInstanceState.getString("q");
		} else {
			Bundle args = this.getArguments();
			query = args.getString(MainActivity.MOVIE_QUERY);
		}

		getActivity().getActionBar().setSubtitle(query);
		getData();
	}

	// interface
	OnMovieListClicked mCallback;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mCallback = (OnMovieListClicked) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnListLoaded");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.list_title, container, false);

		list = (ListView) view.findViewById(R.id.list);
		list.setOnItemClickListener(this);

		progress = (ProgressBar) view.findViewById(R.id.progList);
		txtEmpty = (TextView) view.findViewById(R.id.txtEmpty);

		footer = (LinearLayout) inflater.inflate(R.layout.list_footer, null);
		btnPrev = (Button) footer.findViewById(R.id.btnPrev);
		btnPrev.setOnClickListener(this);

		btnNext = (Button) footer.findViewById(R.id.btnNext);
		btnNext.setOnClickListener(this);

		list.addFooterView(footer, null, false);

		return view;
	}

	@Override
	public void onStart() {
		super.onStart();
		updateNextPrevControls();
	}

	@Override
	public void onSaveInstanceState(Bundle out) {
		super.onSaveInstanceState(out);
		out.putString("q", query);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		requestQueue.cancelAll(this);
	}

	private void getData() {
		String url = String.format(ApiStrings.getTmdbConfigUrl(),
				ApiStrings.getMovieDbKey());

		JsonObjectRequest objRequestPath = new JsonObjectRequest(Request.Method.GET, url,
				null, new ImageResponseListener(), new ErrorListener());

		requestQueue.add(objRequestPath);

		JsonObjectRequest objRequest = new JsonObjectRequest(Request.Method.GET,
				createUrl(), null, new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject response) {
						JsonParser parser = new JsonParser();
						JsonObject jsonObject = parser.parse(response.toString())
								.getAsJsonObject();

						JsonArray movies = jsonObject.get(MainActivity.RESULTS)
								.getAsJsonArray();

						int pages = jsonObject.get(MainActivity.TOTAL_PAGES).getAsInt();

						totalPages = pages;

						Type movieType = new TypeToken<List<Results>>() {
						}.getType();

						Gson gson = new Gson();
						List<Results> movie = gson.fromJson(movies, movieType);

						List<String> allTitles = new ArrayList<String>();
						List<String> allDates = new ArrayList<String>();
						List<String> allPosterPaths = new ArrayList<String>();
						List<Long> mID = new ArrayList<Long>();
						List<Boolean> isSelected = new ArrayList<Boolean>();

						int size = movie.size();
						DateSort[] sortTitles = new DateSort[size];

						for (int i = 0; i < size; i++) {

							String title = movie.get(i).getTitle();
							String date = movie.get(i).getReleaseDate();
							String poster = movie.get(i).getPosterPath() != null ? imageBaseUrl
									+ movie.get(i).getPosterPath()
									: "";
							long theMDBID = movie.get(i).getID();

							DateSort newTitle = new DateSort(title, date, poster, theMDBID,
									FavoriteMovie.isFavoriteMovie(getActivity(),
											String.valueOf(theMDBID)));

							sortTitles[i] = newTitle;
						}

						Arrays.sort(sortTitles, DateSort.yearComparator);

						for (DateSort data : sortTitles) {

							allTitles.add(data.getTitle());
							allDates.add(StringFormatter.parseDate(data.getDate()));
							allPosterPaths.add(data.getImagePath());
							mID.add(data.getID());
							isSelected.add(data.getFav());
						}

						title = allTitles;
						releaseDate = allDates;
						posterPath = allPosterPaths;
						movieID = mID;
						favIsSelected = isSelected;

						if (title.size() == 0) {
							txtEmpty.setVisibility(View.VISIBLE);
						} else {
							if (txtEmpty.getVisibility() == View.VISIBLE)
								txtEmpty.setVisibility(View.GONE);
							list.setAdapter(adapter);
						}

						adapter.notifyDataSetChanged();
						if (progress.getVisibility() == View.VISIBLE)
							progress.setVisibility(View.GONE);

						updateNextPrevControls();
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						Log.e(TAG, "RESPONSE ERROR: " + error.toString());
					}
				});
		requestQueue.add(objRequest);
	}

	private class ImageResponseListener implements Listener<JSONObject> {
		@Override
		public void onResponse(JSONObject response) {
			JsonParser parser = new JsonParser();
			JsonObject jsonObject = parser.parse(response.toString()).getAsJsonObject();
			JsonObject images = jsonObject.get(MainActivity.IMAGES).getAsJsonObject();

			Gson gson = new Gson();

			Type configType = new TypeToken<TmdbConfiguration>() {
			}.getType();

			TmdbConfiguration lConfig = gson.fromJson(images, configType);

			String size = lConfig.getPosterSizes().get(1);
			String sBaseUrl = lConfig.getBaseUrl();
			imageBaseUrl = sBaseUrl + size;
		}
	}

	private class ErrorListener implements Response.ErrorListener {
		@Override
		public void onErrorResponse(VolleyError error) {
			Log.e(TAG, "RESPONSE ERROR: " + error.toString());
		}
	}

	private String createUrl() {
		String sQuery = "";
		String url = ApiStrings.getMovieDbUrl();
		try {
			sQuery = URLEncoder.encode(query, "utf-8");
			url = String.format(url, ApiStrings.getMovieDbKey(), sQuery,
					String.valueOf(currentPage));
		} catch (UnsupportedEncodingException e) {
			Log.e(TAG, e.getMessage());
		}
		return url;
	}

	static class ViewHolder {
		TextView txtTitle;
		TextView txtDesc;
		// NetworkImageView image;
		ImageView imgFav;
	}

	private class ResultsListAdapter extends BaseAdapter {
		public ResultsListAdapter(Context context) {
			mContext = context;
		}

		public int getCount() {
			return title.size();
		}

		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(final int position, View convertView, ViewGroup parent) {

			final ViewHolder holder = new ViewHolder();

			if (convertView == null) {
				convertView = LayoutInflater.from(mContext).inflate(
						R.layout.results_list_item, null);
				convertView.setTag(holder);
			}

			holder.txtTitle = (TextView) convertView.findViewById(R.id.txtTitle);
			holder.txtTitle.setText(title.get(position));
			holder.txtDesc = (TextView) convertView.findViewById(R.id.txtDesc);

			String date = StringFormatter.parseDate(releaseDate.get(position));
			holder.txtDesc.setText(date);

			holder.imgFav = (ImageView) convertView.findViewById(R.id.imgFav);

			NetworkImageView image = (NetworkImageView) convertView
					.findViewById(R.id.imgMovie);
			imageLoader = VolleySingleton.getInstance(getActivity()).getImageLoader();

			boolean isSelected = favIsSelected.get(position);

			final Drawable imgOn = getActivity().getResources().getDrawable(
					R.drawable.ic_fav_on);

			final Drawable imgOff = getActivity().getResources().getDrawable(
					R.drawable.ic_fav_off);

			String imagePath = null;
			if (posterPath != null) {
				imagePath = posterPath.get(position);
			} else {
				imagePath = "";
			}

			image.setDefaultImageResId(R.drawable.no_image);
			image.setImageUrl(imagePath, imageLoader);

			if (isSelected) {
				holder.imgFav.setImageDrawable(imgOn);
			} else {
				holder.imgFav.setImageDrawable(imgOff);
			}

			holder.imgFav.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Long theMDBID = movieID.get(position);
					String movieTitle = title.get(position);
					String thumb = posterPath.get(position);

					boolean wasSelected = favIsSelected.get(position);

					if (wasSelected) {
						favIsSelected.set(position, false);

						holder.imgFav.setImageDrawable(imgOff);
						FavoriteMovie.delete(getActivity(), movieID.get(position));
					} else {
						favIsSelected.set(position, true);

						holder.imgFav.setImageDrawable(imgOn);

						new AddFavTask().execute(theMDBID, movieTitle, thumb);
					}
				}
			});

			return convertView;
		}

		private Context mContext;
	}

	public void doSearch(String sQuery) {
		progress.setVisibility(View.VISIBLE);
		query = sQuery;
		currentPage = 1;
		getData();
	}

	private void doPrevOrNext(int delta) {
		progress.setVisibility(View.VISIBLE);
		currentPage += delta;
		getData();
	}

	private void updateNextPrevControls() {
		boolean showPrev = currentPage > 1;
		boolean showNext = currentPage < totalPages;

		boolean prevIsVisible = btnPrev.getVisibility() == View.VISIBLE;
		boolean nextIsVisible = btnNext.getVisibility() == View.VISIBLE;

		if (showPrev && !prevIsVisible) {
			btnPrev.setVisibility(View.VISIBLE);
		}

		else if (!showPrev && prevIsVisible) {
			btnPrev.setVisibility(View.INVISIBLE);
		}

		if (showNext && !nextIsVisible) {
			btnNext.setVisibility(View.VISIBLE);
		}

		else if (!showNext && nextIsVisible) {
			btnNext.setVisibility(View.INVISIBLE);
		}

		list.setSelection(0);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnPrev:
			doPrevOrNext(-1);
			break;
		case R.id.btnNext:
			doPrevOrNext(1);
			break;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		mCallback.movieTitleClicked(movieID.get(arg2));
	}

	private class AddFavTask extends AsyncTask<Object, Void, Void> {
		@Override
		protected Void doInBackground(Object... params) {

			String urlString = String.format(ApiStrings.getSingleMovie(), params[0],
					ApiStrings.getMovieDbKey());
			String json = ReadStream.readStream(urlString);
			String imdbID = GetImDbID.getMovieResult(json);

			FavoriteMovie.add(getActivity(), (Long) params[0], imdbID, (String) params[1],
					(String) params[2]);
			return null;
		}
	}
}
