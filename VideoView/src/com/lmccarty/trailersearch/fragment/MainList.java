package com.lmccarty.trailersearch.fragment;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.lmccarty.trailersearch.R;
import com.lmccarty.trailersearch.activity.MainActivity;
import com.lmccarty.trailersearch.dao.DatabaseConstants;
import com.lmccarty.trailersearch.model.DvdReleases;
import com.lmccarty.trailersearch.model.EnumDVDReleaseType;
import com.lmccarty.trailersearch.model.EnumDVDReleaseType.DVDReleaseType;
import com.lmccarty.trailersearch.model.FavoriteMovie;
import com.lmccarty.trailersearch.model.Movie;
import com.lmccarty.trailersearch.model.Results;
import com.lmccarty.trailersearch.model.VolleySingleton;
import com.lmccarty.trailersearch.util.AppPrefs;
import com.lmccarty.trailersearch.util.JSONHelper;
import com.lmccarty.trailersearch.util.ReadStream;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupCollapseListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.Toast;

public class MainList extends Fragment implements OnChildClickListener,
		OnGroupCollapseListener, OnGroupExpandListener {

	private static final String TAG = MainList.class.getSimpleName();

	private static final String TMDB_URL = "https://api.themoviedb.org/3/";
	private static final String DVD_URLS = "dvd_urls";
	private static final String DVD_TYPE_HEADER = "dvd_type_header";
	private static final String LIST_EXPANDED = "list_expanded";
	private static final String CHILD = "child";
	private static final String GROUP = "group";
	private static final String CHILD_WAS_SELECTED = "child_was_selected";

	private List<String> dvdTypeHeader;
	private List<String> dvdUrls = new ArrayList<String>();

	private static List<List<String>> dvdMovieTitle = new ArrayList<List<String>>();
	private static List<List<Integer>> dvdMovieYear = new ArrayList<List<Integer>>();
	private static List<List<String>> dvdMovieCritics = new ArrayList<List<String>>();
	private static List<List<String>> dvdMovieThumb = new ArrayList<List<String>>();
	private static List<List<Boolean>> favIsSelected = new ArrayList<List<Boolean>>();

	private static List<List<String>> dvdMoviePosterLarge = new ArrayList<List<String>>();
	private static List<List<String>> dvdMovieSynopsis = new ArrayList<List<String>>();
	private static List<List<String>> dvdMovieSource = new ArrayList<List<String>>();
	private static List<List<Long>> dvdMovieRTID = new ArrayList<List<Long>>();

	private boolean[] isExpanded;
	private boolean childWasSelected = false;
	private int group;
	private int child;

	private int pendingVolleyRequest;

	private RequestQueue requestQueue;
	private BaseExpandableListAdapter adapter;
	private ExpandableListView list;
	private ProgressBar progress;

	private AddFavTask addFavTask;
	private QueryDbTask queryTask;
	private SaveAndGetDataTask saveDataTask;

	OnRTMovieListItemClicked mCallback;

	public interface OnRTMovieListItemClicked {
		public void movieTitleClicked(String imdbID);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mCallback = (OnRTMovieListItemClicked) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnListItemClicked");
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		adapter = new VideoExpandableListAdapter();
		requestQueue = VolleySingleton.getInstance(getActivity()).getRequestQueue();
		dvdTypeHeader = new ArrayList<String>();

		if (savedInstanceState != null) {
			boolean[] expanded = savedInstanceState.getBooleanArray(LIST_EXPANDED);

			String[] header = savedInstanceState.getStringArray(DVD_TYPE_HEADER);
			List<String> lHeader = Arrays.asList(header);

			String[] urls = savedInstanceState.getStringArray(DVD_URLS);
			List<String> lUrls = Arrays.asList(urls);

			boolean clicked = savedInstanceState.getBoolean(CHILD_WAS_SELECTED);
			int iGroup = savedInstanceState.getInt(GROUP);
			int iChild = savedInstanceState.getInt(CHILD);

			isExpanded = expanded;
			dvdTypeHeader = lHeader;
			dvdUrls = lUrls;
			childWasSelected = clicked;
			group = iGroup;
			child = iChild;
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.expandable_list, container, false);

		progress = (ProgressBar) view.findViewById(R.id.progList);
		list = (ExpandableListView) view.findViewById(R.id.exp_list);
		list.setOnChildClickListener(this);
		list.setOnGroupCollapseListener(this);
		list.setOnGroupExpandListener(this);
		list.setTextFilterEnabled(true);

		list.setAdapter(adapter);

		return view;
	}

	@Override
	public void onStart() {
		super.onStart();

		loadState();
		getMovieData();
	}

	@Override
	public void onStop() {
		super.onStop();
		if (queryTask != null && queryTask.getStatus() == AsyncTask.Status.RUNNING) {
			queryTask.cancel(true);
		}

		if (saveDataTask != null && saveDataTask.getStatus() == AsyncTask.Status.RUNNING) {
			saveDataTask.cancel(true);
		}

		clearParentHeader();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		requestQueue.cancelAll(this);
	}

	@Override
	public void onSaveInstanceState(Bundle save) {
		super.onSaveInstanceState(save);
		save.putBooleanArray(LIST_EXPANDED, isExpanded);

		String[] urls = dvdUrls.toArray(new String[dvdUrls.size()]);
		save.putStringArray(DVD_URLS, urls);

		String[] header = dvdTypeHeader.toArray(new String[dvdTypeHeader.size()]);
		save.putStringArray(DVD_TYPE_HEADER, header);

		save.putBoolean(CHILD_WAS_SELECTED, childWasSelected);
		save.putInt(GROUP, group);
		save.putInt(CHILD, child);
	}

	@Override
	public boolean onChildClick(ExpandableListView arg0, View arg1, int arg2, int arg3,
			long arg4) {
		childWasSelected = true;
		child = arg3;
		group = arg2;

		// IMDb ID
		String id = dvdMovieSource.get(arg2).get(arg3);

		if (id.length() == 0) {
			Toast.makeText(getActivity(), getResources().getString(R.string.no_data),
					Toast.LENGTH_LONG).show();

		} else {
			mCallback.movieTitleClicked(dvdMovieSource.get(arg2).get(arg3));
		}
		return false;
	}

	@Override
	public void onGroupCollapse(int groupPosition) {
		isExpanded[groupPosition] = false;
	}

	@Override
	public void onGroupExpand(int groupPosition) {
		isExpanded[groupPosition] = true;
	}

	private void loadState() {
		dvdTypeHeader = new ArrayList<String>();

		setPendingVolleyRequests();

		boolean[] expand = new boolean[pendingVolleyRequest];
		for (int i = 0; i < pendingVolleyRequest; i++) {
			expand[i] = false;
		}

		isExpanded = expand;
	}

	private int decrementPendingRequests() {
		return pendingVolleyRequest--;
	}

	private int getPendingVolleyRequests() {
		return pendingVolleyRequest;
	}

	private void setPendingVolleyRequests() {
		int pendingRequests = DVDReleaseType.values().length;
		pendingVolleyRequest = pendingRequests;
	}

	private void clearParentHeader() {
		dvdTypeHeader.clear();
	}

	private void setParentHeader(String header) {
		dvdTypeHeader.add(header);
	}

	private void getMovieData() {

		if (progress.getVisibility() != View.VISIBLE)
			progress.setVisibility(View.VISIBLE);

		for (final DVDReleaseType type : DVDReleaseType.values()) {

			EnumDVDReleaseType movieData = new EnumDVDReleaseType(getActivity(), type);

			final String listHeader = movieData.dvdReleaseListParent();
			final String tableName = movieData.dvdReleasesTableName();

			setParentHeader(listHeader);

			if (isDataFresh(getActivity())) {
				queryTask = new QueryDbTask();
				queryTask.execute(tableName);
			} else {
				JsonObjectRequest objRequest = new JsonObjectRequest(Request.Method.GET,
						movieData.releaseURL(), null, new Response.Listener<JSONObject>() {
							@Override
							public void onResponse(JSONObject response) {
								try {
									List<Movie> movie = JSONHelper.parseDVDReleases(
											getActivity(), type, response);
									saveDataTask = (SaveAndGetDataTask) new SaveAndGetDataTask();
									saveDataTask.execute(type, movie, tableName);
								} catch (JsonSyntaxException e) {
									Log.e(TAG, "Json syntax error: " + e.getMessage());
								} catch (JSONException e) {
									Log.e(TAG, "JSON error: " + e.getMessage());
								}
							}
						}, new Response.ErrorListener() {
							@Override
							public void onErrorResponse(VolleyError error) {
								Log.e(TAG, "Volley error: " + error.toString());
							}
						});
				requestQueue.add(objRequest);
			} // End else 
		}
	}

	static class ViewHolderParent {
		TextView txtTitle;
	}

	static class ViewHolderChild {
		TextView txtChild;
		TextView txtYear;
		ImageView imgFav;
	}

	public class VideoExpandableListAdapter extends BaseExpandableListAdapter {

		public String getChild(int groupPosition, int childPosition) {
			return dvdMovieTitle.get(groupPosition).get(childPosition);
		}

		public int getYear(int groupPosition, int childPosition) {
			return dvdMovieYear.get(groupPosition).get(childPosition);
		}

		public String getCritics(int groupPosition, int childPosition) {
			return dvdMovieCritics.get(groupPosition).get(childPosition);
		}

		public String getThumb(int groupPosition, int childPosition) {
			return dvdMovieThumb.get(groupPosition).get(childPosition);
		}

		public String getLargePoster(int groupPosition, int childPosition) {
			return dvdMoviePosterLarge.get(groupPosition).get(childPosition);
		}

		public long getChildId(int groupPosition, int childPosition) {
			return childPosition;
		}

		public int getChildrenCount(int groupPosition) {
			return dvdMovieTitle.get(groupPosition).size();
		}

		public View getChildView(final int groupPosition, final int childPosition,
				boolean isLastChild, View convertView, ViewGroup parent) {

			final ViewHolderChild holder = new ViewHolderChild();

			if (convertView == null) {
				convertView = LayoutInflater.from(parent.getContext()).inflate(
						R.layout.expandable_list_child_item, null);
				convertView.setTag(holder);
			}

			holder.txtChild = (TextView) convertView.findViewById(R.id.txtChild);
			holder.txtChild.setText(getChild(groupPosition, childPosition).toString());

			holder.txtYear = (TextView) convertView.findViewById(R.id.txtCritics);
			holder.txtYear.setText(String.valueOf(getYear(groupPosition, childPosition)));

			ImageLoader imageLoader = VolleySingleton.getInstance(getActivity())
					.getImageLoader();

			NetworkImageView image = (NetworkImageView) convertView
					.findViewById(R.id.imgListThumb);

			image.setDefaultImageResId(R.drawable.no_image);
			image.setImageUrl(getThumb(groupPosition, childPosition).toString(),
					imageLoader);

			holder.imgFav = (ImageView) convertView.findViewById(R.id.imgFav);

			boolean isSelected = favIsSelected.get(groupPosition).get(childPosition);

			final Drawable imgOn = getActivity().getResources().getDrawable(
					R.drawable.ic_fav_on);

			final Drawable imgOff = getActivity().getResources().getDrawable(
					R.drawable.ic_fav_off);

			if (isSelected) {
				holder.imgFav.setImageDrawable(imgOn);
			} else {
				holder.imgFav.setImageDrawable(imgOff);
			}

			holder.imgFav.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// IMDb movie ID add "tt" for Rotten Tomatoes
					String imdbID = dvdMovieSource.get(groupPosition).get(childPosition)
							.length() == 0 ? "" : dvdMovieSource.get(groupPosition).get(
							childPosition);

					// Rotten Tomatoes movie ID
					long rtid = dvdMovieRTID.get(groupPosition).get(childPosition);

					String title = dvdMovieTitle.get(groupPosition).get(childPosition);
					String thumb = dvdMovieThumb.get(groupPosition).get(childPosition);

					boolean wasSelected = favIsSelected.get(groupPosition).get(
							childPosition);
					if (wasSelected) {
						favIsSelected.get(groupPosition).set(childPosition, false);
						holder.imgFav.setImageDrawable(imgOff);
						FavoriteMovie.delete(getActivity(), imdbID, rtid);
					} else {
						favIsSelected.get(groupPosition).set(childPosition, true);
						holder.imgFav.setImageDrawable(imgOn);

						addFavTask = (AddFavTask) new AddFavTask();
						addFavTask.execute(imdbID, rtid, title, thumb);
					}
				}
			});

			return convertView;
		}

		public String getGroup(int groupPosition) {
			return dvdTypeHeader.get(groupPosition);
		}

		public int getGroupCount() {
			return dvdTypeHeader.size();
		}

		public long getGroupId(int groupPosition) {
			return groupPosition;
		}

		public View getGroupView(int groupPosition, boolean isExpanded, View convertView,
				ViewGroup parent) {
			LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);

			View row = inflater
					.inflate(R.layout.expandable_list_parent_item, parent, false);

			final ViewHolderParent holder = new ViewHolderParent();

			holder.txtTitle = (TextView) row.findViewById(R.id.txtTitle);
			holder.txtTitle.setText(getGroup(groupPosition));

			return row;
		}

		public boolean isChildSelectable(int groupPosition, int childPosition) {
			return true;
		}

		public boolean hasStableIds() {
			return true;
		}
	}

	public void getTitleData(String type, String query) throws UnsupportedEncodingException {

		String url = TMDB_URL + type + "?query=" + URLEncoder.encode(query, "utf-8")
				+ ApiStrings.getMovieDbKey();

		JsonObjectRequest objRequest = new JsonObjectRequest(Request.Method.GET, url, null,
				new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject response) {
						Log.i(TAG, "OUTPUT: " + response.toString());
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						Log.e(TAG, "RESPONSE ERROR: " + error.toString());
					}
				});
		requestQueue.add(objRequest);
	}

	private class AddFavTask extends AsyncTask<Object, Void, Void> {
		@Override
		protected Void doInBackground(Object... params) {

			String urlString = String.format(ApiStrings.getSingleMovieWithIMDbID(),
					(String) params[0], ApiStrings.getMovieDbKey());

			String json = ReadStream.readStream(urlString);

			FavoriteMovie.add(getActivity(), (String) params[0], (Long) params[1],
					getMovieResult(json), (String) params[2], (String) params[3]);

			return null;
		}
	}

	private long getMovieResult(String json) {
		JsonParser parser = new JsonParser();
		long id = 0;

		try {
			JsonObject jsonObject = parser.parse(json.toString()).getAsJsonObject();

			if (jsonObject.has(MainActivity.MOVIE_RESULTS)) {
				JsonObject movie = (JsonObject) jsonObject.get(MainActivity.MOVIE_RESULTS)
						.getAsJsonArray().get(0);

				Gson gson = new Gson();
				Type movieType = new TypeToken<Results>() {
				}.getType();
				Results result = gson.fromJson(movie, movieType);
				id = result.getID();
			}
		} catch (JsonParseException e) {
			Log.d(TAG, "JsonParseException ERROR: " + e.getMessage());
		} catch (IllegalStateException e) {
			Log.d(TAG, "IllegalStateException ERROR: " + e.getMessage());
		} catch (NullPointerException e) {
			Log.d(TAG, "Missing data: " + e.getMessage());
		}
		return id;
	}

	/*
	 * Check the age of the downloaded dvd release information. If the info is
	 * less than 24 hours old then it is fresh
	 */
	private boolean isDataFresh(Context ctx) {
		long oneDay = 86400000L;
		long now = System.currentTimeMillis();
		long age = AppPrefs.getDownloadedDvdReleaseAge(ctx);
		long difference = now - age;

		if (difference <= oneDay)
			return true;

		return false;
	}

	private void setLastDownloadTime(Context ctx) {
		AppPrefs.setLastDownloadTime(ctx);
	}

	/*
	 * type, movie, tableName
	 */
	private class SaveAndGetDataTask extends AsyncTask<Object, Void, String> {
		@Override
		protected void onPostExecute(String param) {
			queryTask = new QueryDbTask();
			queryTask.execute(param);
		}

		@Override
		protected String doInBackground(Object... params) {

			DVDReleaseType type = (DVDReleaseType) params[0];

			@SuppressWarnings("unchecked")
			List<Movie> movie = (List<Movie>) params[1];
			String tableName = (String) params[2];

			switch (type) {
			case CURRENT_RELEASES:
				DvdReleases.deleteAllRows(getActivity(),
						DatabaseConstants.CURRENT_RELEASES_TABLE);
				DvdReleases.add(getActivity(), movie,
						DatabaseConstants.CURRENT_RELEASES_TABLE);
				break;
			case NEW_RELEASES:
				DvdReleases.deleteAllRows(getActivity(),
						DatabaseConstants.NEW_RELEASES_TABLE);
				DvdReleases.add(getActivity(), movie, DatabaseConstants.NEW_RELEASES_TABLE);
				break;
			case TOP_RENTALS:
				DvdReleases.deleteAllRows(getActivity(),
						DatabaseConstants.TOP_RENTALS_TABLE);
				DvdReleases.add(getActivity(), movie, DatabaseConstants.TOP_RENTALS_TABLE);
				break;
			case UPCOMING:
				DvdReleases.deleteAllRows(getActivity(), DatabaseConstants.UPCOMING_TABLE);
				DvdReleases.add(getActivity(), movie, DatabaseConstants.UPCOMING_TABLE);
				break;
			}

			return tableName;
		}
	}

	private class QueryDbTask extends AsyncTask<String, Void, DvdReleases> {
		@Override
		protected void onPostExecute(DvdReleases releases) {
			List<String> title = releases.getTitleList();
			List<Integer> year = releases.getYearList();
			List<String> thumb = releases.getThumbList();
			List<String> imdbID = releases.getImdbIDList();
			List<Long> tomatoID = releases.getTomatoIDList();
			List<String> critics = releases.getCriticsList();
			List<String> poster = releases.getPosterList();
			List<String> synopsis = releases.getSynopsisList();
			List<Boolean> favSelectedList = new ArrayList<Boolean>();

			for (String isFav : imdbID) {
				favSelectedList.add(FavoriteMovie.isFavoriteMovieCheckIMDB(getActivity(),
						isFav));
			}

			dvdMovieTitle.add(title);
			dvdMovieYear.add(year);
			dvdMovieCritics.add(critics);
			dvdMovieThumb.add(thumb);
			dvdMoviePosterLarge.add(poster);
			dvdMovieSynopsis.add(synopsis);
			dvdMovieSource.add(imdbID);
			dvdMovieRTID.add(tomatoID);
			favIsSelected.add(favSelectedList);

			decrementPendingRequests();

			if (getPendingVolleyRequests() == 0) {
				setLastDownloadTime(getActivity());

				adapter.notifyDataSetChanged();

				if (progress.getVisibility() == View.VISIBLE) {
					progress.setVisibility(View.GONE);
				}
			}
		}

		@Override
		protected DvdReleases doInBackground(String... param) {
			DvdReleases releases = DvdReleases.getDvdReleases(getActivity(), param[0]);
			return releases;
		}
	}
}
