package com.lmccarty.trailersearch.util;

import com.lmccarty.trailersearch.R;
import android.app.Activity;
import android.content.Context;
import android.view.View;

public class UIUtils {

	private UIUtils() {
		throw new AssertionError();
	}

	public static boolean isRightInLayout(Context con) {
		View rightFrame = ((Activity) con).findViewById(R.id.container_right);
		if (rightFrame == null) {
			return false;
		} else {
			return true;
		}
	}
}
