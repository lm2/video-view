package com.lmccarty.trailersearch.dao;

import java.util.ArrayList;
import java.util.List;

import com.lmccarty.trailersearch.model.FavoriteActor;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import static android.provider.BaseColumns._ID;

public class FavoriteActorDAO extends DAOHelper {

	private static final String TAG = FavoriteActorDAO.class.getSimpleName();

	public FavoriteActorDAO(Context ctx) {
		super(ctx, DATABASE_NAME, null, DATABASE_VERSION);
	}

	public void addFavoriteActor(FavoriteActor actor) {
		SQLiteDatabase db = getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(ACTOR_ID, actor.getActorID());
		values.put(ACTOR_NAME, actor.getName());
		values.put(THUMB, actor.getThumb());
		long id = db.insertOrThrow(THE_MDB_ACTOR_FAV, null, values);

		if (id > 0) {
			Log.i(TAG, "New favorite saved: " + actor.getName());
		}
	}

	public boolean isFavoriteActor(Long id) {

		boolean fav = false;
		Cursor cursor = null;

		try {
			SQLiteDatabase db = getReadableDatabase();

			String sql = "SELECT " + _ID + " FROM " + THE_MDB_ACTOR_FAV + " WHERE " + ACTOR_ID
					+ " = " + id;

			cursor = db.rawQuery(sql, null);
			int count = cursor.getCount();

			if (count > 0) {
				fav = true;
			}
		} finally {
			closeCursor(cursor);
		}

		return fav;
	}

	public void deleteFavoriteActor(String id) {
		String where = ACTOR_ID + " = " + id;
		SQLiteDatabase db = getWritableDatabase();
		int success = db.delete(THE_MDB_ACTOR_FAV, where, null);

		if (success > 0)
			Log.i(TAG, "Deleted actor ID: " + id);
	}

	public List<List<String>> getFavoriteActors() {

		List<List<String>> l = null;
		Cursor cursor = null;
		try {
			SQLiteDatabase db = getWritableDatabase();

			String sql = "SELECT " + ACTOR_NAME + "," + ACTOR_ID + "," + THUMB + " FROM "
					+ THE_MDB_ACTOR_FAV + " ORDER BY " + ACTOR_NAME + " COLLATE NOCASE ASC";

			cursor = db.rawQuery(sql, null);

			List<String> name = new ArrayList<String>();
			List<String> actorID = new ArrayList<String>();
			List<String> thumb = new ArrayList<String>();

			while (cursor.moveToNext()) {
				name.add(cursor.getString(0));
				actorID.add(cursor.getString(1));
				thumb.add(cursor.getString(2));
			}

			l = new ArrayList<List<String>>();
			l.add(name);
			l.add(actorID);
			l.add(thumb);
		} finally {
			closeCursor(cursor);
		}

		return l;
	}
}
