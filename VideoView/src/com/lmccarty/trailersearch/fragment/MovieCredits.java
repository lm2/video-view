package com.lmccarty.trailersearch.fragment;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.json.JSONObject;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.lmccarty.trailersearch.R;
import com.lmccarty.trailersearch.activity.MainActivity;
import com.lmccarty.trailersearch.model.Cast;
import com.lmccarty.trailersearch.model.FavoriteMovie;
import com.lmccarty.trailersearch.model.OnMovieListClicked;
import com.lmccarty.trailersearch.model.VolleySingleton;
import com.lmccarty.trailersearch.util.AppPrefs;
import com.lmccarty.trailersearch.util.DateSort;
import com.lmccarty.trailersearch.util.GetImDbID;
import com.lmccarty.trailersearch.util.ReadStream;
import com.lmccarty.trailersearch.util.StringFormatter;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MovieCredits extends Fragment implements OnItemClickListener {

	private static final String TAG = MovieCredits.class.getSimpleName();

	private long actorID = 0;
	private String actorName = null;
	private String configUrl = null;

	private List<String> title = new ArrayList<String>();
	private List<String> posterPath = new ArrayList<String>();
	private List<String> releaseDate = new ArrayList<String>();
	private List<Long> theMDbID = new ArrayList<Long>();

	private List<Boolean> favIsSelected;

	private TextView txtEmpty;
	private ProgressBar progress;
	private ListView list;
	private ListAdapter adapter;
	private RequestQueue requestQueue;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setHasOptionsMenu(true);

		requestQueue = VolleySingleton.getInstance(getActivity()).getRequestQueue();
		adapter = new ResultsListAdapter(getActivity());

		// TODO is savedInstanceState needed?
		if (savedInstanceState != null) {
			actorID = savedInstanceState.getLong("id");
			actorName = savedInstanceState.getString("name");
		} else {
			Bundle args = this.getArguments();
			actorID = args.getLong(MainActivity.ACTOR_ID);
			actorName = args.getString(MainActivity.ACTOR_NAME);

			getActivity().getActionBar().setTitle(
					getActivity().getResources().getString(R.string.app_name));
			getActivity().getActionBar().setSubtitle(actorName);
		}

		configUrl = AppPrefs.getTmdbConfigUrl(getActivity());

		getData();
	}

	// interface
	OnMovieListClicked mCallback;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mCallback = (OnMovieListClicked) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnActionRequired");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.list_title, container, false);

		list = (ListView) view.findViewById(R.id.list);
		list.setOnItemClickListener(this);

		progress = (ProgressBar) view.findViewById(R.id.progList);
		txtEmpty = (TextView) view.findViewById(R.id.txtEmpty);

		return view;
	}

	@Override
	public void onSaveInstanceState(Bundle out) {
		super.onSaveInstanceState(out);
		out.putLong("id", actorID);
		out.putString("name", actorName);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		requestQueue.cancelAll(this);
	}

	private void getData() {
		String fullUrl = String.format(ApiStrings.getActorCreditsUrl(), actorID,
				ApiStrings.getMovieDbKey());

		JsonObjectRequest objRequest = new JsonObjectRequest(Request.Method.GET, fullUrl,
				null, new ResponseListener(), new ErrorListener());
		requestQueue.add(objRequest);
	}

	private class ResponseListener implements Listener<JSONObject> {
		@Override
		public void onResponse(JSONObject response) {
			JsonParser parser = new JsonParser();
			JsonObject jsonObject = parser.parse(response.toString()).getAsJsonObject();

			JsonArray movies = jsonObject.get("cast").getAsJsonArray();

			Type type = new TypeToken<List<Cast>>() {
			}.getType();

			Gson gson = new Gson();
			List<Cast> movie = gson.fromJson(movies, type);

			List<String> movieTitle = new ArrayList<String>();
			List<String> movieDate = new ArrayList<String>();
			List<String> moviePosterPath = new ArrayList<String>();
			List<Long> mID = new ArrayList<Long>();
			List<Boolean> isSelected = new ArrayList<Boolean>();

			List<DateSort> sortTitles = new ArrayList<DateSort>();

			for (Cast data : movie) {
				String title = data.getTitle();
				String date = data.getReleaseDate() == null ? "" : data.getReleaseDate();
				String poster = (data.getPosterPath() != null ? configUrl
						+ data.getPosterPath() : "");

				long id = data.getID();

				DateSort newTitle = new DateSort(title, date, poster, id,
						FavoriteMovie.isFavoriteMovie(getActivity(), String.valueOf(id)));
				sortTitles.add(newTitle);
			}

			Collections.sort(sortTitles, DateSort.yearComparator);

			for (DateSort data : sortTitles) {
				movieTitle.add(data.getTitle());
				movieDate.add(StringFormatter.parseDate(data.getDate()));
				moviePosterPath.add(data.getImagePath());
				mID.add(data.getID());
				isSelected.add(data.getFav());
			}

			title = movieTitle;
			posterPath = moviePosterPath;
			releaseDate = movieDate;
			theMDbID = mID;
			favIsSelected = isSelected;

			if (progress.getVisibility() != View.GONE)
				progress.setVisibility(View.INVISIBLE);

			if (movie.size() == 0) {
				if (txtEmpty.getVisibility() != View.VISIBLE)
					txtEmpty.setVisibility(View.VISIBLE);
			} else {
				list.setAdapter(adapter);
			}
		}
	}

	private class ErrorListener implements Response.ErrorListener {
		@Override
		public void onErrorResponse(VolleyError error) {
			Log.e(TAG, "RESPONSE ERROR: " + error.toString());
		}
	}

	static class ViewHolder {
		TextView txtTitle;
		TextView txtDesc;
		// NetworkImageView image;
		ImageView imgFav;
	}

	private class ResultsListAdapter extends BaseAdapter {
		public ResultsListAdapter(Context context) {
			mContext = context;
		}

		public int getCount() {
			return title.size();
		}

		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(final int position, View convertView, ViewGroup parent) {

			if (convertView == null) {
				convertView = LayoutInflater.from(mContext).inflate(
						R.layout.results_list_item, null);
			}

			final ViewHolder holder = new ViewHolder();

			holder.txtTitle = (TextView) convertView.findViewById(R.id.txtTitle);
			holder.txtTitle.setText(title.get(position));

			holder.txtDesc = (TextView) convertView.findViewById(R.id.txtDesc);
			holder.txtDesc.setText(releaseDate.get(position));

			NetworkImageView image = (NetworkImageView) convertView
					.findViewById(R.id.imgMovie);

			ImageLoader imageLoader = VolleySingleton.getInstance(getActivity())
					.getImageLoader();

			String posterImage = posterPath.get(position);

			image.setDefaultImageResId(R.drawable.no_image);

			if (posterImage.length() > 0) {
				image.setImageUrl(posterImage, imageLoader);
			}

			boolean isSelected = favIsSelected.get(position);

			final Drawable imgOn = getActivity().getResources().getDrawable(
					R.drawable.ic_fav_on);

			final Drawable imgOff = getActivity().getResources().getDrawable(
					R.drawable.ic_fav_off);

			holder.imgFav = (ImageView) convertView.findViewById(R.id.imgFav);

			if (isSelected) {
				holder.imgFav.setImageDrawable(imgOn);
			} else {
				holder.imgFav.setImageDrawable(imgOff);
			}

			holder.imgFav.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					String id = String.valueOf(theMDbID.get(position));
					String thumb = posterPath.get(position);
					String sTitle = title.get(position);

					boolean wasSelected = favIsSelected.get(position);

					if (wasSelected) {
						favIsSelected.set(position, false);

						holder.imgFav.setImageDrawable(imgOff);

						FavoriteMovie.delete(getActivity(), theMDbID.get(position));

					} else {
						favIsSelected.set(position, true);

						holder.imgFav.setImageDrawable(imgOn);

						new AddFavTask().execute(id, sTitle, thumb);
					}
				}
			});

			return convertView;
		}

		private Context mContext;
	}

	public void doSearch(String sQuery) {
		progress.setVisibility(View.VISIBLE);
		getData();
	}

	public void updateUI(String aName, long id) {
		actorName = aName;
		actorID = id;
		getActivity().getActionBar().setSubtitle(actorName);
		getData();
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		mCallback.movieTitleClicked(theMDbID.get(arg2));
	}

	private class AddFavTask extends AsyncTask<String, Void, Void> {
		@Override
		protected Void doInBackground(String... params) {
			long theMDBID = Long.valueOf(params[0]);
			String urlString = String.format(ApiStrings.getSingleMovie(), theMDBID,
					ApiStrings.getMovieDbKey());

			String json = ReadStream.readStream(urlString);
			String imdbID = GetImDbID.getMovieResult(json);
			String title = params[1];
			String thumb = params[2];

			FavoriteMovie.add(getActivity(), theMDBID, imdbID, title, thumb);

			return null;
		}
	}

	public void clearListOnNewIntent() {
		if (progress.getVisibility() != View.VISIBLE)
			progress.setVisibility(View.VISIBLE);

		((BaseAdapter) list.getAdapter()).notifyDataSetChanged(); 
	}
}
