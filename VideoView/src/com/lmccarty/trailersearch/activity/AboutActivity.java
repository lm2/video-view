package com.lmccarty.trailersearch.activity;

import com.lmccarty.trailersearch.fragment.About;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;

public class AboutActivity extends Activity {

	private static final String ABOUT = "about_frag";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		FragmentManager manager = getFragmentManager();
		FragmentTransaction trans = manager.beginTransaction();

		About frag = new About();

		trans.replace(android.R.id.content, frag, ABOUT).commit();
	}
}