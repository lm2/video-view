package com.lmccarty.trailersearch.model;

import java.util.List;

public class TmdbConfiguration {

	private String base_url;
	private String secure_base_url;

	private List<String> backdrop_sizes;
	private List<String> logo_sizes;
	private List<String> poster_sizes;
	private List<String> profile_sizes;
	private List<String> still_sizes;
	
	public TmdbConfiguration() {
	}

	public void setBaseUrl(String url) {
		base_url = url;
	}

	public String getBaseUrl() {
		return base_url;
	}

	public void setSecureBaseUrl(String url) {
		secure_base_url = url;
	}

	public String getSecureBaseUrl() {
		return secure_base_url;
	}

	public void setBackdropSizes(List<String> bd) {
		backdrop_sizes = bd;
	}

	public List<String> getBackdropSizes() {
		return backdrop_sizes;
	}

	public void setLogoSizes(List<String> logo) {
		logo_sizes = logo;
	}

	public List<String> getLogoSizes() {
		return logo_sizes;
	}

	public void setPosterSizes(List<String> poster) {
		poster_sizes = poster;
	}

	public List<String> getPosterSizes() {
		return poster_sizes;
	}

	public void setProfileSizes(List<String> profile) {
		profile_sizes = profile;
	}

	public List<String> getProfileSizes() {
		return profile_sizes;
	}

	public void setStillSizes(List<String> still) {
		still_sizes = still;
	}

	public List<String> getStillSizes() {
		return still_sizes;
	}
}