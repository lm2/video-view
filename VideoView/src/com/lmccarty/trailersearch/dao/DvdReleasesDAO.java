package com.lmccarty.trailersearch.dao;

import java.util.ArrayList;
import java.util.List;

import com.lmccarty.trailersearch.model.DvdReleases;
import com.lmccarty.trailersearch.model.Movie;
import com.lmccarty.trailersearch.util.Utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

public class DvdReleasesDAO extends DAOHelper {

	private static final String TAG = DvdReleasesDAO.class.getSimpleName();

	public DvdReleasesDAO(Context ctx) {
		super(ctx, DATABASE_NAME, null, DATABASE_VERSION);
	}

	public DvdReleasesDAO addDvdReleases(List<Movie> movie, String table) {
		SQLiteDatabase db = getReadableDatabase();

		ContentValues values = null;

		for (Movie data : movie) {
			values = new ContentValues();
			values.put(TITLE, data.getTitle());
			values.put(YEAR, String.valueOf(data.getYear()));
			values.put(THUMB_PATH, data.getPosters().getThumbnail());
			values.put(CRITICS_CONSENSUS, data.getCritics());
			values.put(POSTER_PATH, data.getPosters().getOriginal());
			values.put(SYNOPSIS, data.getSynopsis());
			values.put(ALTERNATE_ID,
					data.getAlternateIDs() != null ? Utils.imdbIDPrefix()
							+ data.getAlternateIDs().getIMDB() : "");
			values.put(TOMATO_ID, String.valueOf(data.getID()));

			try {
				db.insertOrThrow(table, null, values);
			} catch (SQLiteException e) {
				Log.i(TAG, "Insertion error: " + e.getMessage());
			}
		}

		return null;
	}

	public DvdReleases getDvdReleases(Context ctx, String table) {

		DvdReleases releases = null;

		Cursor cursor = null;
		String[] columns = { TITLE, YEAR, THUMB_PATH, ALTERNATE_ID, TOMATO_ID,
				CRITICS_CONSENSUS, POSTER_PATH, SYNOPSIS };
		try {
			SQLiteDatabase db = getReadableDatabase();
			cursor = db.query(table, columns, null, null, null, null, TITLE
					+ " COLLATE NOCASE ASC");
			releases = createReleasesFromCursorData(cursor);
		} catch (SQLiteException e) {
			Log.e(TAG, "Could not query db. " + e.getMessage());
		} finally {
			closeCursor(cursor);
		}

		return releases;
	}

	private DvdReleases createReleasesFromCursorData(Cursor cursor) {

		List<String> title = new ArrayList<String>();
		List<Integer> year = new ArrayList<Integer>();
		List<String> thumb = new ArrayList<String>();
		List<String> imdbID = new ArrayList<String>();
		List<Long> tomatoID = new ArrayList<Long>();
		List<String> critics = new ArrayList<String>();
		List<String> poster = new ArrayList<String>();
		List<String> synopsis = new ArrayList<String>();

		while (cursor.moveToNext()) {
			title.add(cursor.getString(0));
			year.add(cursor.getInt(1));
			thumb.add(cursor.getString(2));
			imdbID.add(cursor.getString(3));
			tomatoID.add(cursor.getLong(4));
			critics.add(cursor.getString(5));
			poster.add(cursor.getString(6));
			synopsis.add(cursor.getString(7));
		}
		return new DvdReleases(title, year, thumb, imdbID, tomatoID, critics, poster,
				synopsis);
	}

	public void deleteAllRows(String table) {
		SQLiteDatabase db = getWritableDatabase();
		int rows = db.delete(table, null, null);
		Log.i(TAG, "Rows deleted: " + rows);
	}
}
