package com.lmccarty.trailersearch.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.util.Log;

public class StringFormatter {

	private static final String TAG = "StringFormatter";

	private StringFormatter() {
		throw new AssertionError();
	}

	public static String parseDate(String d) {
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat fmtYear = new SimpleDateFormat("yyyy");
		try {
			Date date = fmt.parse(d);
			return fmtYear.format(date);
		} catch (ParseException e) {
			Log.d(TAG, "COULDN'T PARSE DATE: " + e.getMessage());
			return d;
		}
	}
}
