package com.lmccarty.trailersearch.model;

public interface OnActorListClicked {
	public void showMovieCredits(Long actorID, String actorName);
}
