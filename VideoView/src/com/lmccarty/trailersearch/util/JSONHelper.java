package com.lmccarty.trailersearch.util;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;
import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.lmccarty.trailersearch.activity.MainActivity;
import com.lmccarty.trailersearch.model.MovieDetails;
import com.lmccarty.trailersearch.model.EnumDVDReleaseType.DVDReleaseType;
import com.lmccarty.trailersearch.model.Movie;
import com.lmccarty.trailersearch.model.YouTube;

public class JSONHelper {

	private static final String TAG = JSONHelper.class.getSimpleName();

	private JSONHelper() {
		throw new AssertionError();
	}

	public static List<Movie> parseDVDReleases(Context ctx, DVDReleaseType type,
			JSONObject response) throws JsonSyntaxException, JSONException {

		JsonParser parser = new JsonParser();
		JsonObject jsonObject = parser.parse(response.toString()).getAsJsonObject();
		JsonArray movies = jsonObject.get("movies").getAsJsonArray();

		Gson gson = new Gson();

		Type movieType = new TypeToken<List<Movie>>() {
		}.getType();

		List<Movie> movie = gson.fromJson(movies, movieType);

		return movie;
	}

	@SuppressLint("UseSparseArrays")
	public static MovieDetails parseMovieDetails(Context ctx, JSONObject response) {

		String title = null;
		String overview = null;
		String imdbID = null;
		String posterPath = null;
		Map<Integer, String> genreMap = new HashMap<Integer, String>();

		long theMDbID = 0L;

		JsonParser parser = new JsonParser();
		JsonObject jsonObject = null;
		JsonArray genres = null;

		try {
			jsonObject = parser.parse(response.toString()).getAsJsonObject();
			title = jsonObject.get("title").getAsString();
			overview = jsonObject.has("overview")
					&& !jsonObject.get("overview").isJsonNull() ? jsonObject
					.get("overview").getAsString() : null;
			theMDbID = jsonObject.get("id").getAsLong();
			imdbID = jsonObject.has("imdb_id") && !jsonObject.get("imdb_id").isJsonNull() ? jsonObject
					.get("imdb_id").getAsString() : null;

			if (jsonObject.has("poster_path") && MainActivity.tmdbConfigUrl != null
					&& (!jsonObject.get("poster_path").isJsonNull())) {

				posterPath = MainActivity.tmdbConfigUrl
						+ jsonObject.get("poster_path").getAsString();
			}

			if (jsonObject.has("genres") && !jsonObject.get("genres").isJsonNull()) {
				genres = jsonObject.get("genres").getAsJsonArray();
				for (JsonElement data : genres) {
					genreMap.put(data.getAsJsonObject().get("id").getAsInt(), data
							.getAsJsonObject().get("name").getAsString());
				}
			}
		} catch (JsonSyntaxException e) {
			Log.e(TAG, "Error: " + e.getMessage());
		}
		return new MovieDetails.Builder(title, theMDbID, imdbID).posterPath(posterPath)
				.overview(overview).genres(genreMap).build();
	}

	public static List<YouTube> getTrailers(JSONObject response) {
		List<YouTube> youtube = new ArrayList<YouTube>();

		JsonParser parser = new JsonParser();
		JsonObject jsonObject = parser.parse(response.toString()).getAsJsonObject();
		JsonArray trailerArray = jsonObject.get("youtube").getAsJsonArray();

		Gson gson = new Gson();

		try {

			Type trailerType = new TypeToken<List<YouTube>>() {
			}.getType();
			List<YouTube> youtubeList = gson.fromJson(trailerArray, trailerType);
			youtube.addAll(youtubeList);
		} catch (JsonParseException e) {
			Log.e(TAG, "ERROR: " + e.getMessage());
		}

		return youtube;
	}
}
