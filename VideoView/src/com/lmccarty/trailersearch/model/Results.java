package com.lmccarty.trailersearch.model;

public class Results {

	private boolean adult;
	private long id;
	private long vote_count;
	private double popularity;
	private double vote_average;
	private String backdrop_path;
	private String original_title;
	private String release_date;
	private String poster_path;
	private String title;

	public void setAdult(boolean b) {
		adult = b;
	}

	public boolean getAdult() {
		return adult;
	}

	public void setID(long lid) {
		id = lid;
	}

	public long getID() {
		return id;
	}

	public void setVoteCount(long vote) {
		vote_count = vote;
	}

	public long getVoteCount() {
		return vote_count;
	}

	public void setPopularity(double pop) {
		popularity = pop;
	}

	public double getPopularity() {
		return popularity;
	}

	public void setVoteAverage(double ave) {
		vote_average = ave;
	}

	public double getVoteAverage() {
		return vote_average;
	}

	public void setBackdropPath(String path) {
		backdrop_path = path;
	}

	public String getBackdropPath() {
		return backdrop_path;
	}

	public void setOriginalTitle(String title) {
		original_title = title;
	}

	public String getOriginalTitle() {
		return original_title;
	}

	public void setReleaseDate(String date) {
		release_date = date;
	}

	public String getReleaseDate() {
		return release_date;
	}

	public void setPosterPath(String path) {
		poster_path = path;
	}

	public String getPosterPath() {
		return poster_path;
	}

	public void setTitle(String t) {
		title = t;
	}

	public String getTitle() {
		return title;
	}
}
