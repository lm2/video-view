package com.lmccarty.trailersearch.model;

import java.util.List;

public class MovieList {

	private static List<List<String>> movies;
	private static List<List<String>> critics;

	public void setMovieList(List<List<String>> list) {
		movies = list;
	}

	public List<List<String>> getMovieList() {
		return movies;
	}

	public void setCritics(List<List<String>> list) {
		critics = list;
	}

	public List<List<String>> getCritics() {
		return critics;
	}
}
