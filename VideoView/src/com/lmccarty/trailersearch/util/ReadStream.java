package com.lmccarty.trailersearch.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import android.util.Log;

public class ReadStream {
	private ReadStream() {
		throw new AssertionError();
	}

	private static final String TAG = ReadStream.class.getSimpleName();

	public static String readStream(String urlString) {
		URL url;
		HttpURLConnection con = null;
		BufferedReader br = null;
		String json = null;
		try {
			url = new URL(urlString);
			con = (HttpURLConnection) url.openConnection();
			con.setReadTimeout(10000);
			con.setConnectTimeout(15000);
			con.setRequestMethod("GET");
			con.setDoInput(true);
			con.connect();

			int status = con.getResponseCode();

			if (status == 200) {
				br = new BufferedReader(new InputStreamReader(
						con.getInputStream()));
				String line;
				StringBuilder builder = new StringBuilder();

				while ((line = br.readLine()) != null) {
					builder.append(line);
				}
				json = builder.toString();
			}
		} catch (MalformedURLException e) {
			Log.d(TAG, e.getMessage());
		} catch (IOException e) {
			Log.d(TAG, e.getMessage());
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					Log.d(TAG, e.getMessage());
				}
			}
		}
		return json;
	}
}
