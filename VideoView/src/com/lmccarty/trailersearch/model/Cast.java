package com.lmccarty.trailersearch.model;

/**
 * 
 * The results when a user searches for a movie title
 * 
 */
public class Cast {

	private long id;
	private String title;
	private String character;
	private String original_title;
	private String poster_path;
	private String release_date;
	private boolean adult;

	public void setID(long lid) {
		id = lid;
	}

	public long getID() {
		return id;
	}

	public void setOriginalTitle(String title) {
		original_title = title;
	}

	public String getOriginalTitle() {
		return original_title;
	}

	public void setCharacter(String ch) {
		character = ch;
	}

	public String getCharacter() {
		return character;
	}

	public void setReleaseDate(String date) {
		release_date = date;
	}

	public String getReleaseDate() {
		return release_date;
	}

	public void setPosterPath(String path) {
		poster_path = path;
	}

	public String getPosterPath() {
		return poster_path;
	}

	public void setTitle(String t) {
		title = t;
	}

	public String getTitle() {
		return title;
	}

	public void setAdult(boolean b) {
		adult = b;
	}

	public boolean getAdult() {
		return adult;
	}
}